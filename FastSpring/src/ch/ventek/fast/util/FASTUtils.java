package ch.ventek.fast.util;

import java.util.Collection;

import com.fastsearch.esp.search.result.IQueryResult;
import com.fastsearch.esp.search.result.IQueryTransformation;
import com.fastsearch.esp.search.result.IQueryTransformations;


public class FASTUtils {
	
	public static String purifyQuery(String query) {
		if (query == null)
			return "";
		
		String purifiedQuery = query;
		
		purifiedQuery = purifiedQuery.replaceAll("\"", "\\\\\"");
		purifiedQuery = purifiedQuery.replaceAll("[*?]", "");
		
		return purifiedQuery;
	}
	
	public static String escapeFQL(String str) {
		return str.replaceAll("\"", "\\\\\"");
	}
	
	public static String getDidyoumean(IQueryResult resultSets) {
		IQueryTransformations iTransF = resultSets.getQueryTransformations(false);
		String didyoumean = null;
		if (iTransF.getSuggestion(IQueryTransformations.DID_YOU_MEAN) != null) {
			Collection<IQueryTransformation> trans = iTransF.getSuggestions(IQueryTransformations.DID_YOU_MEAN);
			for(IQueryTransformation qt : trans){
				if(qt.getMessageID() == 14) {
					didyoumean = qt.getQuery();
					break;
				}
			}
		}
		return didyoumean;
	}
}
