package ch.ventek.fast.search;

import java.util.Properties;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.fastsearch.esp.search.ConfigurationException;
import com.fastsearch.esp.search.ISearchFactory;
import com.fastsearch.esp.search.SearchEngineException;
import com.fastsearch.esp.search.SearchFactory;
import com.fastsearch.esp.search.view.ISearchView;

public class FASTSearchView {
	private ISearchView view = null;
	private String fastHostPort;
	private String fastView;
	
	private static final Log logger = LogFactory.getLog(FASTSearchView.class);

	public FASTSearchView(String fastHostPort, String fastView) {
		this.fastHostPort = fastHostPort;
		this.fastView = fastView;
		try {
			Properties p = new Properties();
			p.setProperty("com.fastsearch.esp.search.SearchFactory", "com.fastsearch.esp.search.http.HttpSearchFactory");
			p.setProperty("com.fastsearch.esp.search.http.qrservers", fastHostPort);
			ISearchFactory searchFactory = SearchFactory.newInstance(p);
	
			view = searchFactory.getSearchView(fastView, true);
			logger.info("SearchView created");
			logger.info("  fastHostPort :" + fastHostPort);
			logger.info("  fastView     :" + fastView);
		} catch (ConfigurationException e) {
			e.printStackTrace();
		} catch (SearchEngineException e) {
			e.printStackTrace();
		}
	}
	
	public ISearchView getView() {
		return view;
	}
	
	public String getFastHostPort() {
		return fastHostPort;
	}

	public String getFastView() {
		return fastView;
	}
}
