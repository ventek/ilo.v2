package ch.ventek.fast.search;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;
import org.xml.sax.helpers.XMLReaderFactory;

import ch.ventek.fast.domain.AutoSuggestEntry;

public class FASTAutoSuggestXML extends FASTAutoSuggest {
	private static final Log logger = LogFactory.getLog(FASTAutoSuggestXML.class);
	
	public ArrayList<AutoSuggestEntry> suggest(String s) {
		ArrayList<AutoSuggestEntry> suggestions = new ArrayList<AutoSuggestEntry>();
		try {
			s = URLEncoder.encode(s, "UTF-8");
			URL url = new URL("http", this.fastASHost, this.fastASPort, "/search?q=" + s);
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			InputStream in = connection.getInputStream();
			InputSource source = new InputSource( in );
			
			XMLReader parser = XMLReaderFactory.createXMLReader();
			parser.setContentHandler(new AutoSuggestXMLHandler(suggestions));
			parser.parse(source);
			in.close();
			connection.disconnect();
		} catch (MalformedURLException e) {
			logger.warn("Hacking attempt!! q: " + s);
		} catch (IOException e) {} 
		catch (SAXException e) {
			logger.warn("XML Parsing error");
		}
		return suggestions;
	}

	protected class AutoSuggestXMLHandler extends DefaultHandler {
		ArrayList<AutoSuggestEntry> suggestions;
		AutoSuggestEntry entry;
		
		public AutoSuggestXMLHandler(ArrayList<AutoSuggestEntry> suggestions) {
			this.suggestions = suggestions;
		}
		
		@Override
		public void startElement(String uri, String localName, String qName, Attributes attributes)	throws SAXException {
			if ( "match".equals(localName) ) {
				this.entry = new AutoSuggestEntry();
				this.entry.setStart( Integer.parseInt( attributes.getValue("start") ) );
				this.entry.setStop( Integer.parseInt( attributes.getValue("stop") ) );
				this.entry.setQuality( Integer.parseInt( attributes.getValue("quality") ) );
                if (attributes.getValue("meta") != null)
				    this.entry.setMeta( Integer.parseInt( attributes.getValue("meta") ) );
                if (attributes.getValue("score") != null)
				    this.entry.setScore( Integer.parseInt( attributes.getValue("score") ) );
			}
		}
		
		@Override
		public void characters(char[] ch, int start, int length) throws SAXException {
			if ( this.entry != null )
				this.entry.setBase( new String(ch, start, length) );
		}
		
		@Override
		public void endElement(String uri, String localName, String qName) throws SAXException {
			if ( this.entry != null ) {
				this.suggestions.add( this.entry );
				this.entry = null;
			}
		}
	}
}
