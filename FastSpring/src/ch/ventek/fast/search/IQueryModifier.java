package ch.ventek.fast.search;

import com.fastsearch.esp.search.query.IQuery;

public interface IQueryModifier {
	public void prepareQuery(IQuery query);
}
