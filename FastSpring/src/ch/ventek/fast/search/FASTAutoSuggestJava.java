package ch.ventek.fast.search;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import ch.ventek.fast.domain.AutoSuggestEntry;

public class FASTAutoSuggestJava extends FASTAutoSuggest {
	private static final Log logger = LogFactory.getLog(FASTAutoSuggestJava.class);
	
	public ArrayList<AutoSuggestEntry> suggest(String s) {
		ArrayList<AutoSuggestEntry> suggestions = new ArrayList<AutoSuggestEntry>();
		try {
			s = URLEncoder.encode(s, "UTF-8");
			URL url = new URL("http", this.fastASHost, this.fastASPort, "/search?q=" + s);
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			BufferedReader in =  new BufferedReader( new InputStreamReader( connection.getInputStream(), "UTF-8" ) );
			String line = in.readLine();
			if ( line.startsWith("[") && line.endsWith("]") ) {
				line = line.substring(1, line.length() - 1);
				int stage = 0;
				AutoSuggestEntry entry = new AutoSuggestEntry();
				for ( String chunk : line.split(",") ) {
					chunk = chunk.substring(1, chunk.length() - 1);
					switch (stage) {
					case 0:
						entry.setBase( chunk );
						break;
					case 1:
						if ( chunk.length() > 0 )
							entry.setStop( Integer.parseInt(chunk) );
						suggestions.add(entry);
						entry = new AutoSuggestEntry();
						break;
					}
					stage = (stage + 1) % 2;
				}
			}
			
			in.close();
			connection.disconnect();
		} catch (MalformedURLException e) {
			logger.warn("Hacking attempt!! q: " + s);
		} catch (IOException e) {
			
		}
		

		return suggestions;
	}
	

	protected class AutoSuggestXMLHandler extends DefaultHandler {
		ArrayList<AutoSuggestEntry> suggestions;
		AutoSuggestEntry entry;
		
		public AutoSuggestXMLHandler(ArrayList<AutoSuggestEntry> suggestions) {
			this.suggestions = suggestions;
		}
		
		@Override
		public void startElement(String uri, String localName, String qName, Attributes attributes)	throws SAXException {
			if ( "match".equals(localName) ) {
				this.entry = new AutoSuggestEntry();
				this.entry.setStart( Integer.parseInt( attributes.getValue("start") ) );
				this.entry.setStop( Integer.parseInt( attributes.getValue("stop") ) );
				this.entry.setQuality( Integer.parseInt( attributes.getValue("quality") ) );
				this.entry.setMeta( Integer.parseInt( attributes.getValue("meta") ) );
				this.entry.setScore( Integer.parseInt( attributes.getValue("score") ) );
			}
		}
		
		@Override
		public void characters(char[] ch, int start, int length) throws SAXException {
			if ( this.entry != null )
				this.entry.setBase( new String(ch, start, length) );
		}
		
		@Override
		public void endElement(String uri, String localName, String qName) throws SAXException {
			if ( this.entry != null ) {
				this.suggestions.add( this.entry );
				this.entry = null;
			}
		}
	}
}
