package ch.ventek.fast.search;

import java.util.ArrayList;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import ch.ventek.fast.domain.AutoSuggestEntry;

public abstract class FASTAutoSuggest {
	private static final Log logger = LogFactory.getLog(FASTAutoSuggest.class);
	
	protected String fastASHostPort;
	protected String fastASHost;
	protected int fastASPort;
	
	public FASTAutoSuggest() {
	}
	
	public abstract ArrayList<AutoSuggestEntry> suggest(String s);
	
	public String getFastASHostPort() {
		return fastASHostPort;
	}

	public void setFastASHostPort(String fastASHostPort) {
		this.fastASHostPort = fastASHostPort;
		int i = this.fastASHostPort.indexOf(':');
		this.fastASHost = this.fastASHostPort.substring(0, i);
		this.fastASPort = Integer.parseInt(this.fastASHostPort.substring(i+1));
	}
}
