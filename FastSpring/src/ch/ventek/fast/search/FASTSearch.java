package ch.ventek.fast.search;

import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.fastsearch.esp.search.navigation.INavigation;
import com.fastsearch.esp.search.query.BaseParameter;
import com.fastsearch.esp.search.query.IQuery;
import com.fastsearch.esp.search.query.Query;
import com.fastsearch.esp.search.query.SearchParameter;
import com.fastsearch.esp.search.result.IQueryResult;
import com.fastsearch.esp.search.view.ISearchView;

/**
 * @author Ake Tangkananond 
 *
 */
public class FASTSearch {
	private static final Log logger = LogFactory.getLog(FASTSearch.class);
	private static final Pattern fqlReplacementPattern = Pattern.compile("\\{\\d+\\}");
	
	private FASTSearchView searchView;
	private String fastLanguage = "en";
	private String fastEncoding;
	private String fastShowNavigators = "false";
	private String fastLemmatize = "false";
	private String fastSortBy;
	private boolean fastSortReversed = false;
	private String fastQueryPattern;
	private String fastCollapseOn;
	private IQueryModifier customQueryModifier;


	protected String constructQueryString(Map<String, String> search) {
		String pattern = fastQueryPattern;
		
		if (pattern == null || pattern.trim().length() == 0) {
			logger.warn("Query pattern is not assigned. Use default pattern");
			pattern = "string(\"{0}\", mode=simpleall, annotation_class=\"user\")";
		}
		
		StringBuffer sbFql = new StringBuffer();
		Matcher matcher = fqlReplacementPattern.matcher(pattern);
		
		while (matcher.find()) {
			String val = search.get(matcher.group());
			if (val == null) {
				logger.warn("Use empty string for null key : " + matcher.group());
				val = "";
			}
			matcher.appendReplacement(sbFql, Matcher.quoteReplacement(val));
		}
		matcher.appendTail(sbFql);

		logger.info("Query transforming...");
		logger.info("pattern  : " + pattern);
		logger.info("fql      : " + sbFql);
		
		return sbFql.toString();
	}
	
	protected void prepareQuery(IQuery query, int offset, int hits) {
		query.setParameter(new SearchParameter(BaseParameter.LANGUAGE, fastLanguage));
		query.setParameter(new SearchParameter(BaseParameter.ENCODING, fastEncoding));
		query.setParameter(new SearchParameter(BaseParameter.NAVIGATION, fastShowNavigators));
		query.setParameter(new SearchParameter(BaseParameter.OFFSET, offset));
		query.setParameter(new SearchParameter(BaseParameter.HITS, hits));
		query.setParameter(new SearchParameter(BaseParameter.LEMMATIZE, fastLemmatize));
		
		if (fastSortBy != null) {
			query.setParameter(new SearchParameter(BaseParameter.SORT_BY, fastSortBy));
		}
		if (fastSortReversed == true) {
			query.setParameter(new SearchParameter(BaseParameter.SORT_DIRECTION, BaseParameter.ASCENDING));
		}
		if (fastCollapseOn != null) {
			query.setParameter(new SearchParameter("collapseon", fastCollapseOn));
		}
		if (customQueryModifier != null) {
			customQueryModifier.prepareQuery(query);
		}
	}


	public IQueryResult search(Map<String, String> search, int offset, int hits) {
		String fql = constructQueryString(search);
		IQueryResult resultSet = search(fql, offset, hits);
		return resultSet;
	}

	public IQueryResult search(String fql, int offset, int hits) {
		IQueryResult resultSet = search(fql, offset, hits, null);
		return resultSet;
	}
	
	public IQueryResult search(String fql, int offset, int hits, List<SearchParameter> searchParams) {
		IQueryResult resultSet = search(fql, null, offset, hits, null);
		return resultSet;
	}

	public IQueryResult search(Map<String, String> search, INavigation navigation, int offset, int hits) {
		IQueryResult resultSet = search(search, navigation, offset, hits, null);
		return resultSet;
	}
	
	public IQueryResult search(Map<String, String> search, INavigation navigation, int offset, int hits, List<SearchParameter> searchParams) {
		String fql = constructQueryString(search);
		IQueryResult resultSet = search(fql, navigation, offset, hits, searchParams);
		return resultSet;
	}

	public IQueryResult search(String fql, INavigation navigation, int offset, int hits) {
		IQueryResult resultSet = search(fql, navigation, offset, hits, null);
		return resultSet;
	}
	
	public IQueryResult search(String fql, INavigation navigation, int offset, int hits, List<SearchParameter> searchParams) {
		IQueryResult resultSet = null;

		try {
			ISearchView view = searchView.getView();
			
			IQuery _query = new Query(fql);
			prepareQuery(_query, offset, hits);
			if ( searchParams != null ) {
				for ( SearchParameter s : searchParams )
					_query.setParameter(s);
			}
			
			IQuery query;
			if ( navigation != null ) {
				query = new Query(_query);
				navigation.instrument(query);
			}
			else {
				query = _query;
			}
			
			logger.debug("Querying FAST...");
			logger.debug("fastHostPort     : " + searchView.getFastHostPort());
			logger.debug("fastView         : " + searchView.getFastView());
			logger.debug("fastQueryString  : " + fql);
			logger.debug("fastQueryURL     : " + view.getRequestParameters(query));
			resultSet = view.search(query);
		} catch (Exception e) {
			logger.error("Cannot query FAST", e);
		}

		return resultSet;
	}

	public void setFastLanguage(String fastLanguage) {
		this.fastLanguage = fastLanguage;
	}

	public void setFastEncoding(String fastEncoding) {
		this.fastEncoding = fastEncoding;
	}

	public void setFastShowNavigators(String fastShowNavigators) {
		this.fastShowNavigators = fastShowNavigators;
	}

	public void setFastLemmatize(String fastLemmatize) {
		this.fastLemmatize = fastLemmatize;
	}

	public void setFastSortBy(String fastSortBy) {
		this.fastSortBy = fastSortBy;
	}
	
	public void setFastQueryPattern(String fastQueryPattern) {
		this.fastQueryPattern = fastQueryPattern;
	}
	
	public void setFastCollapseOn(String fastCollapseOn) {
		this.fastCollapseOn = fastCollapseOn;
	}
	
	public void setFastSortReversed(boolean fastSortReversed) {
		this.fastSortReversed = fastSortReversed;
	}

	public void setCustomQueryModifier(IQueryModifier customQueryModifier) {
		this.customQueryModifier = customQueryModifier;
	}
	
	public void setSearchView(FASTSearchView searchView) {
		this.searchView = searchView;
	}
}
