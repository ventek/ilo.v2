package ch.ventek.fast.domain;

public class HttpRequestQuery {
	private String searchIn;
	private String searchWhat;
	private String navigators;
	private String extras;
	private int hits;
	private int offset;
	
	public HttpRequestQuery() {
		searchIn = "";
		searchWhat = "";
		navigators = "";
		extras = "";
		hits = 10;
		offset = 0;
	}
	
	public String getSearchIn() {
		return searchIn;
	}
	public void setSearchIn(String searchIn) {
		this.searchIn = searchIn;
	}
	public String getSearchWhat() {
		return searchWhat;
	}
	public void setSearchWhat(String searchWhat) {
		this.searchWhat = searchWhat;
	}
	public String getNavigators() {
		return navigators;
	}
	public void setNavigators(String navigators) {
		this.navigators = navigators;
	}
	public int getHits() {
		return hits;
	}
	public void setHits(int hits) {
		this.hits = hits;
	}
	public int getOffset() {
		return offset;
	}
	public void setOffset(int offset) {
		this.offset = offset;
	}
	public String getExtras() {
		return extras;
	}
	public void setExtras(String extras) {
		this.extras = extras;
	}
}
