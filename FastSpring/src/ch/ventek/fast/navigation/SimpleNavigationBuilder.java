package ch.ventek.fast.navigation;


import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import com.fastsearch.esp.search.navigation.INavigation;
import com.fastsearch.esp.search.navigation.Navigation;

public class SimpleNavigationBuilder implements INavigationBuilder {

	protected static final char SEPERATOR_RECORD = 30;
	protected static final char SEPERATOR_GROUP = 29;
	
	public List<Modifier> extractModifiers(String navstr) {
		List<Modifier> modifiers = new ArrayList<Modifier>();
		
		if (navstr != null) {
			StringTokenizer tokenizer = new StringTokenizer(navstr, SEPERATOR_RECORD + "");
			while (tokenizer.hasMoreTokens()) {
				String modstr = tokenizer.nextToken();
				String [] mods = modstr.split(SEPERATOR_GROUP + "");
				if (mods.length == 4) {
					Modifier mod = new Modifier();
					mod.navName = mods[0];
					mod.navAttr = mods[1];
					mod.modName = mods[2];
					mod.modValue = mods[3];
					
					modifiers.add(mod);
				}
			}
		}
		
		return modifiers;
	}

	public INavigation getNavigation(String navstr) {
		return getNavigation(extractModifiers(navstr));
	}

	public INavigation getNavigation(List<Modifier> modifiers) {
		Navigation navigation = new Navigation();
		
		for (Modifier modifier : modifiers)
			navigation.add(modifier.navName, modifier.navAttr).add(modifier.modName, modifier.modValue);			
		
		return navigation;
	}
	
	public String packModifiers(List<Modifier> modifiers) {
		if (modifiers == null)
			return "";
		else {
			StringBuffer buf = new StringBuffer();
	
			for (Modifier modifier : modifiers) {
				String modstr = modifier.navName + SEPERATOR_GROUP + modifier.navAttr + SEPERATOR_GROUP + modifier.modName + SEPERATOR_GROUP + modifier.modValue;
				buf.append(modstr);
				buf.append(SEPERATOR_RECORD);
			}
			
			if (buf.length() > 0)
				return buf.substring(0, buf.length() - 1);
			else
				return "";
		}
	}

}
