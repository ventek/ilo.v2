package ch.ventek.fast.navigation;

import java.util.List;

import com.fastsearch.esp.search.navigation.INavigation;

public interface INavigationBuilder {
	
	abstract List<Modifier> extractModifiers(String navstr);
	abstract String packModifiers(List<Modifier> modifiers);
	
	abstract INavigation getNavigation(String navstr);
	abstract INavigation getNavigation(List<Modifier> modifiers);
	
}
