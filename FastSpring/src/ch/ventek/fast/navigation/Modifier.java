package ch.ventek.fast.navigation;

import com.fastsearch.esp.search.result.IModifier;
import com.fastsearch.esp.search.result.INavigator;



/**
 * @author Ake Tangkananond
 * @version 1.00
 */
public class Modifier {
	protected static final Modifier EMPTY_MODIFIER = new Modifier();
	
	public String navName;
	public String navAttr;
	public String modName;
	public String modValue;
	
	public Modifier() {
		navName = "";
		navAttr = "";
		modName = "";
		modValue = "";
	}
	
	public Modifier(IModifier modifier) {
		INavigator navigator = modifier.getNavigator();
		navName = navigator.getName();
		navAttr = navigator.getFieldName();
		modName = modifier.getName();
		modValue = modifier.getValue();
	}

	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		else if (obj instanceof Modifier) {
			Modifier rhs = (Modifier) obj;	
			return navName.equals(rhs.navName) 
				&& navAttr.equals(rhs.navAttr) 
				&& modName.equals(rhs.modName)
				&& modValue.equals(rhs.modValue);
		}
		else {
			return false;
		}
	}
	
	public boolean isEmpty() {
		return EMPTY_MODIFIER.equals(this);
	}
	
	public String toString() {
		return navName + ":" + navAttr + ":" + modName + ":" + modValue;
	}

	public String getNavName() {
		return navName;
	}

	public void setNavName(String navName) {
		this.navName = navName;
	}

	public String getNavAttr() {
		return navAttr;
	}

	public void setNavAttr(String navAttr) {
		this.navAttr = navAttr;
	}

	public String getModName() {
		return modName;
	}

	public void setModName(String modName) {
		this.modName = modName;
	}

	public String getModValue() {
		return modValue;
	}

	public void setModValue(String modValue) {
		this.modValue = modValue;
	}
	
}
