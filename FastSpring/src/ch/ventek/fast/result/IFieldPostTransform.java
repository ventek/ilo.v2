package ch.ventek.fast.result;

import java.util.Map;

public interface IFieldPostTransform {

	abstract void process(Map<String, Object> doc);
	
}
