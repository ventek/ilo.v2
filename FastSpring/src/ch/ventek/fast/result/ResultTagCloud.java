package ch.ventek.fast.result;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Map;

import ch.ventek.fast.result.ResultNavigation.ModifierItem;
import ch.ventek.fast.result.ResultNavigation.NavigatorItem;


public class ResultTagCloud {
	private String navigator;
	private int groups;
	private int length;
	
	public enum SortMode { SORT_RANDOM, SORT_ALPHABETICALLY };
	
	public NavigatorItem getTagCloudEqualElements(Map<String, NavigatorItem> navigation, boolean remove, SortMode sortMode) {
		return getTagCloudEqualElements(navigation, remove, sortMode, "");
	}
	
	public NavigatorItem getTagCloudEqualElements(Map<String, NavigatorItem> navigation, boolean remove, SortMode sortMode, String exclude) {
		NavigatorItem _navItem = navigation.get( this.navigator );
		if ( _navItem == null )
			return null;
		
		NavigatorItem navItem = new ResultNavigation.NavigatorItem();
		navItem.id = _navItem.id;
		navItem.name = _navItem.name;
		navItem.navigator = _navItem.navigator;
		navItem.modifiers = new ArrayList<ModifierItem>();
		
		// get only [this.length] and exclude tag cloud contained in the 'exclude'
		int i, cnt = 0;
		for ( i = 0; i < _navItem.modifiers.size(); i++ ) {
			if ( ! exclude.contains( (String) _navItem.modifiers.get(i).getName() ) ) {
				navItem.modifiers.add(_navItem.modifiers.get(i));
				cnt++;
			}
			if ( cnt == this.length )
				break;
		}
		
		int len = navItem.modifiers.size();
		int groupSize = len / this.groups;
		
		if ( groupSize > 0 ) {
			for ( i = 0; i < len; i++ )
				navItem.modifiers.get(i).group = Math.min(i / groupSize + 1, groupSize);
		}
		else {
			for ( i = 0; i < len; i++ )
				navItem.modifiers.get(i).group = Math.min(i + 1, groupSize);
		}
		
		if ( sortMode == SortMode.SORT_RANDOM )
			sortRandom(navItem, len);
		else if ( sortMode == SortMode.SORT_ALPHABETICALLY )
			sortAlphabetically(navItem, len);
		
		if ( remove ) {
			navigation.remove(navigator);
		}
		
		return navItem;
	}
	
	public void sortRandom(NavigatorItem navItem, int len) {
		int i, j;
		ResultNavigation.ModifierItem tmp;
		
		for ( i = 0; i < len; i++ ) {
			j = i + (int)(Math.random() * (len - i));
			tmp = navItem.modifiers.get(i);
			navItem.modifiers.set(i, navItem.modifiers.get(j));
			navItem.modifiers.set(j, tmp);
		}
	}
	
	public void sortAlphabetically(NavigatorItem navItem, int len) {
		Collections.sort(navItem.modifiers, new Comparator<ModifierItem>() {
			public int compare(ModifierItem o1, ModifierItem o2) {
				String name1 = null, name2 = null;
				
				if ( o1.getName() instanceof String )
					name1 = (String) o1.getName();
				else
					throw new IllegalArgumentException();
				
				if ( o2.getName() instanceof String )
					name2 = (String) o2.getName();
				else
					throw new IllegalArgumentException();
				
				return name1.compareTo(name2);
			}
		});
	}

	public String getNavigator() {
		return navigator;
	}

	public void setNavigator(String navigator) {
		this.navigator = navigator;
	}

	public int getGroups() {
		return groups;
	}

	public void setGroups(int groups) {
		this.groups = groups;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}
}
