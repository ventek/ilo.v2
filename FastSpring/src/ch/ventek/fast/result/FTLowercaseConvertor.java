package ch.ventek.fast.result;


public class FTLowercaseConvertor implements IFieldTransform {

	public Object process(Object obj) {
		if (obj == null)
			return null;
		else {
			if (obj instanceof String)
				return ((String)obj).toLowerCase();
			else
				return null;
		}
	}

}
