package ch.ventek.fast.result;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import ch.ventek.fast.navigation.INavigationBuilder;
import ch.ventek.fast.navigation.Modifier;

import com.fastsearch.esp.search.result.IModifier;
import com.fastsearch.esp.search.result.INavigator;
import com.fastsearch.esp.search.result.IQueryResult;

/**
 * 
 * Example:
 * 	<c:forEach items="${results.navigation}" var="navigationEntry">
		<c:set var="navigation" value="${navigationEntry.value}"/>
		<h2 class="top"><c:out value="${navigation.name}"/></em></h2>
		<ul>
			<c:forEach items="${navigation.modifiers}" var="modifier">
				<li>
					<c:url value="result.htm" var="url">
						<c:param name="searchWhat" value="${httpQuery.searchWhat}"/>
						<c:param name="searchIn" value="${httpQuery.searchIn}"/>
						<c:param name="navigators" value="${modifier.refinestr}"/>
					</c:url>
					<a href="${url}">${modifier.name} (${modifier.count})</a>
				</li>
			</c:forEach>
		</ul>
	</c:forEach>
 * @author Ake Tangkananond
 *
 */
public class ResultNavigation {

	private List<String> navigators;
	private Map<String, String> navigatorDisplayName;
	private Map<String, IFieldTransform> fieldTransform;

	public ResultNavigation() {
		super();
		
		navigators = null;
		navigatorDisplayName = null;
		fieldTransform = null;
	}
		
	public Map<String, NavigatorItem> getNavigation(IQueryResult resultSets, String navstr, INavigationBuilder builder) {
		Map<String, NavigatorItem> navigation = new LinkedHashMap<String, NavigatorItem>();
		List<Modifier> modifiers = builder.extractModifiers(navstr);
		
		// get a list of navigators to be displayed
		List<String> navigators = this.navigators;
		if (navigators == null) {
			navigators = new ArrayList<String>(resultSets.navigatorCount());
			Iterator<String> itr = resultSets.navigatorNames();
			while (itr.hasNext()) {
				navigators.add(itr.next());
			}
		}

		// loop through each navigator
		for (String navigator : navigators) {
			NavigatorItem navItem = new NavigatorItem();
			navItem.navigator = resultSets.getNavigator(navigator);
			
			if (navItem.navigator != null) {
				navItem.id = navItem.navigator.getName();
				
				// get modifier field transformer
				IFieldTransform transformer = null;
				if (fieldTransform != null) {
					if (fieldTransform.containsKey(navigator)) {
						transformer = fieldTransform.get(navigator);
					}
				}
				
				// get navigators' display name
				if (navigatorDisplayName != null) {
					if (navigatorDisplayName.containsKey(navigator))
						navItem.name = navigatorDisplayName.get(navigator);
					else
						navItem.name = navItem.navigator.getDisplayName();
				}
				else
					navItem.name = navItem.navigator.getDisplayName();
				
				// get navigators' modifiers
				navItem.modifiers = new ArrayList<ModifierItem>(navItem.navigator.modifierCount());
				Iterator<IModifier> modItr = navItem.navigator.modifiers();
				while (modItr.hasNext()) {
					IModifier mod = modItr.next();
					
					List<Modifier> _modifiers = new  ArrayList<Modifier>(modifiers.size() + 1);
					_modifiers.addAll(modifiers);
					
					Modifier newmod = new Modifier(mod);
					if (!_modifiers.contains(newmod))
						_modifiers.add(newmod);
					
					ModifierItem modItem = new ModifierItem();
					modItem.modifier = newmod;
					if (transformer == null)
						modItem.name = mod.getName();
					else {
						modItem.name = transformer.process(mod.getName());
					}
					modItem.count = mod.getCount();
					modItem.refinestr = builder.packModifiers(_modifiers);
					navItem.modifiers.add(modItem);
				}
				
				navigation.put(navigator, navItem);
			}
		}
		
		return navigation;
	}
	
	public void setNavigators(List<String> navigators) {
		this.navigators = navigators;
	}

	public void setNavigatorDisplayName(Map<String, String> navigatorDisplayName) {
		this.navigatorDisplayName = navigatorDisplayName;
	}
	
	public void setFieldTransform(Map<String, IFieldTransform> fieldTransform) {
		this.fieldTransform = fieldTransform;
	}
	
	public static class NavigatorItem {
		public INavigator navigator;
		
		public String id;
		public String name;
		public List<ModifierItem> modifiers;
		
		public String getId() {
			return id;
		}
		public String getName() {
			return name;
		}
		public List<ModifierItem> getModifiers() {
			return modifiers;
		}
	}
	
	public static class ModifierItem {
		public Modifier modifier;
		
		public Object name;
		public int count;
		public int group;
		public String refinestr;
		
		public Object getName() {
			return name;
		}
		public int getCount() {
			return count;
		}
		public String getRefinestr() {
			return refinestr;
		}
		public Modifier getModifier() {
			return modifier;
		}
		public int getGroup() {
			return group;
		}
	}
}
