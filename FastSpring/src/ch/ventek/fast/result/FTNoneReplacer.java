package ch.ventek.fast.result;


public class FTNoneReplacer implements IFieldTransform {

	private Object replacer;
	
	public Object process(Object obj) {
		if (obj == null || obj.toString().trim().length() == 0)
			return replacer;
		else
			return obj;
	}
	
	public Object getReplacer() {
		return replacer;
	}

	public void setReplacer(Object replacer) {
		this.replacer = replacer;
	}

}
