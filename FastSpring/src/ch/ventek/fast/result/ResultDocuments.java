package ch.ventek.fast.result;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.fastsearch.esp.search.result.EmptyValueException;
import com.fastsearch.esp.search.result.IDocumentSummary;
import com.fastsearch.esp.search.result.IDocumentSummaryField;
import com.fastsearch.esp.search.result.IQueryResult;
import com.fastsearch.esp.search.result.IllegalType;
import com.fastsearch.esp.search.view.FieldType;

public class ResultDocuments {
	private static final Log logger = LogFactory.getLog(ResultDocuments.class);
	private static SimpleDateFormat fastDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
	
	private Map<String, String> fieldMapping;
	private Map<String, IFieldTransform> fieldTransform;
	private List<IFieldPostTransform> fieldPostTransform;
	
	public ResultDocuments() {
		super();
		
		fieldMapping = null;
		fieldTransform = null;
		fieldPostTransform = null;
	}
	
	protected Object getDocFieldValue(IDocumentSummary doc, String field) {
		IDocumentSummaryField sfield = (IDocumentSummaryField) doc.getSummaryField(field);
		Object ret = null;
		
		if (sfield != null && !sfield.isEmpty()) {
			try {
				if (FieldType.DOUBLE.equals(sfield.getType())) {
					ret = sfield.getDoubleValue();
				}
				else if (FieldType.FLOAT.equals(sfield.getType())) {
					ret = sfield.getFloatValue();
				}
				else if (FieldType.INTEGER.equals(sfield.getType())) {
					ret = sfield.getIntValue();
				}
				else if (FieldType.STRING.equals(sfield.getType())) {
					ret = sfield.getStringValue();
				}
				else if (FieldType.DATETIME.equals(sfield.getType())) {
					ret = fastDateFormat.parse(sfield.getStringValue());
				}
				else {
					ret = null;
					logger.warn("Unimplemented type conversion detected.");
				}
			}
			catch (ParseException e) {
				ret = null;
				logger.error("Invalid date format detected.");
			}
			catch (IllegalType e) {
				ret = null;
				logger.error(e);
			} 
			catch (EmptyValueException e) {	
				ret = null;
			}
		}
		
		return ret;
	}

	public List<Map<String, Object>> parseDocuments(IQueryResult resultSet, int offset, int hits) {
		
		List<Map<String, Object>> documents = new ArrayList<Map<String, Object>>(hits);
		int docCount = resultSet.getDocCount();
		
		if (docCount > 0) {
			int begin = offset + 1;
			int end = Math.min(offset + hits, docCount) + 1;
			int index = begin;
			
			while (index < end) {
				IDocumentSummary sdoc = (IDocumentSummary) resultSet.getDocument(index);
				logger.trace(sdoc);
				
				Map<String, Object> doc = new HashMap<String, Object>();
				doc.put("no", sdoc.getDocNo());
				
				for (String target : fieldMapping.keySet()) {
					String source = fieldMapping.get(target);
					Object val = getDocFieldValue(sdoc, source);
				
					if (fieldTransform != null) {
						if (fieldTransform.containsKey(target)) {
							IFieldTransform transformer = fieldTransform.get(target);
							val = transformer.process(val);
						}
					}
					
					doc.put(target, val);
				}
				
				if (fieldPostTransform != null) {
					for (IFieldPostTransform transformer : fieldPostTransform) {
						transformer.process(doc);
					}
				}
				
				documents.add(doc);
				index++;
			}
		}
		
		return documents;
	}
	
	public Map<String, String> getFieldMapping() {
		return fieldMapping;
	}

	public void setFieldMapping(Map<String, String> fieldMapping) {
		this.fieldMapping = fieldMapping;
	}

	public Map<String, IFieldTransform> getFieldTransform() {
		return fieldTransform;
	}

	public void setFieldTransform(Map<String, IFieldTransform> fieldTransform) {
		this.fieldTransform = fieldTransform;
	}

	public List<IFieldPostTransform> getFieldPostTransform() {
		return fieldPostTransform;
	}

	public void setFieldPostTransform(List<IFieldPostTransform> fieldPostTransform) {
		this.fieldPostTransform = fieldPostTransform;
	}
}
