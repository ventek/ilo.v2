package ch.ventek.fast.result;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class FTHTMLStriper implements IFieldTransform {
	
	public Object process(Object obj) {
		if (obj == null) {
			return null;
		}
		else if (! (obj instanceof String)){
			return null;
		}
		else {
			String s = (String) obj;
			
			if (s != null && s.length() > 0) {
				s = s.replace("<b>", "FASTBOLDTEXTFAST");
				s = s.replace("</b>", "FASTENDBOLDTEXTFAST");

				s = s.replaceAll("<.+?>", "");
				s = s.replaceAll(".+?>", "...");
				s = s.replaceAll("<.+?", "...");

				s = s.replace("FASTBOLDTEXTFAST", "<b>");
				s = s.replace("FASTENDBOLDTEXTFAST", "</b>");
			}
			
			if ( s.contains("Measurement, Patterns and") )
				System.out.println(s);
			
			// normalize unbalanced <b> tag
			Pattern ptn = Pattern.compile("<(.*?)>");
			Matcher m = ptn.matcher(s);
			StringBuffer sb = new StringBuffer();
			int cnt = 0;
			while ( m.find() ) {
				if ( "<b>".equals( m.group() ) )
					cnt++;
				else if ( "</b>".equals( m.group() ) )
					cnt--;
				
				if ( cnt > 1 ) {
					m.appendReplacement(sb, "</b><b>");
					cnt = 1;
				}
				else if ( cnt < 0 ) {
					m.appendReplacement(sb, "");
					cnt = 0;
				}
				else
					m.appendReplacement(sb, m.group());
			}
			m.appendTail(sb);
			
			if ( cnt > 0 )
				sb.append("</b>");
			
			return sb.toString();
		}
	}
	
}
