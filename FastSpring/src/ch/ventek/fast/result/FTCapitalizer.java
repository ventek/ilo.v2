package ch.ventek.fast.result;

public class FTCapitalizer implements IFieldTransform {

	public Object process(Object obj) {
		if (obj == null)
			return null;
		else {
			if (obj instanceof String) {
				String s = (String) obj;
				if (s.trim().length() > 1)
					return s.substring(0,1).toUpperCase() + s.substring(1);
				else if (s.trim().length() > 0)
					return s.substring(0,1).toUpperCase();
				else
					return "";
			}
			else
				return null;
		}
	}

}
