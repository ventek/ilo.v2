package ch.ventek.fast.result;


public class FTNullReplacer implements IFieldTransform {

	private Object replacer;
	
	public Object process(Object obj) {
		if (obj == null)
			return replacer;
		else
			return obj;
	}
	
	public Object getReplacer() {
		return replacer;
	}

	public void setReplacer(Object replacer) {
		this.replacer = replacer;
	}

}
