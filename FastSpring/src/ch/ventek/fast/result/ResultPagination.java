package ch.ventek.fast.result;

import java.util.ArrayList;
import java.util.List;

import com.fastsearch.esp.search.result.IQueryResult;

/*
 * 
 * Example:
 * 
 * ** To print the number of shown results
 * 	${results.pagination.current.itemBegin} - 
 * 	${results.pagination.current.itemEnd} of
 * 	${results.pagination.itemTotal} results.
 *	<c:if test="${results.pagination.next != null}">
 *		<a class="seemore" href="/?offset=${results.pagination.next.offset}">Next &gt;&gt;</a>
 *	</c:if>
 * 
 * 
 * ** To print the page listing
 * 	<c:if test="${fn:length(results.pagination.pages) > 0}">
 *	 	<c:forEach items="${results.pagination.pages}" var="page">
 * 			<c:choose>
 * 				<c:when test="${page.current}">
 * 					<span>${page.page}</span>
 * 				</c:when>
 * 				<c:otherwise>
 * 					<a href="/?offset=${page.offset}">${page.page}</a>
 * 				</c:otherwise>
 * 			</c:choose>
 * 		</c:forEach>
 * 	</c:if>
 */
public class ResultPagination {
	
	private int pages;
	private int itemPerPage;
	
	public PaginationItem getPagination(int itemTotal, int offset, int itemPerPage) {
		List<PageItem> pagination_enum = new ArrayList<PageItem>(pages);
		PageItem pagination_current = null;
		PageItem pagination_next = null;
		PageItem pagination_prev = null;
		
		int pageTotal = (itemTotal > 0) ? Math.max(0, (itemTotal - 1) / itemPerPage + 1) : 0;
		
		int pageCurrent = offset / itemPerPage + 1;
		int pageBegin = pageCurrent - pages / 2 + 1;
		int pageEnd = pageCurrent + pages / 2;
		
		if (pageBegin < 1) {
			pageEnd = Math.min(pages, pageTotal);
			pageBegin = 1;
		}
		else if (pageEnd > pageTotal) {
			pageBegin = Math.max(pageTotal - pages + 1, 1);
			pageEnd = pageTotal;
		}
		
		if ( pageBegin > 1 ) {
			int page = pageBegin - 1;
			PageItem pageItem = new PageItem();
			pageItem.page = page;
			pageItem.offset = (page - 1) * itemPerPage;
			pageItem.isCurrent = page == pageCurrent;
			pageItem.itemBegin = (page - 1) * itemPerPage + 1;
			pageItem.itemEnd = Math.min(pageItem.itemBegin + itemPerPage - 1, itemTotal);
			pagination_enum.add(pageItem);
			
			pagination_prev = pageItem;
		}
		for (int page = pageBegin; page <= pageEnd; page++) {
			PageItem pageItem = new PageItem();
			pageItem.page = page;
			pageItem.offset = (page - 1) * itemPerPage;
			pageItem.isCurrent = page == pageCurrent;
			pageItem.itemBegin = (page - 1) * itemPerPage + 1;
			pageItem.itemEnd = Math.min(pageItem.itemBegin + itemPerPage - 1, itemTotal);
			pagination_enum.add(pageItem);
			
			if (page == pageCurrent)
				pagination_current = pageItem;
			else if (page - 1 == pageCurrent)
				pagination_next = pageItem;
		}
		
		PaginationItem paginationItem = new PaginationItem();
		paginationItem.pages = pagination_enum;
		paginationItem.current = pagination_current;
		paginationItem.next = pagination_next;
		paginationItem.prev = pagination_prev;
		paginationItem.itemTotal = itemTotal;
		
		return paginationItem;
	}
	
	public PaginationItem getPagination(IQueryResult resultSet, int offset, int itemPerPage) {
		return getPagination(resultSet.getDocCount(), offset, itemPerPage);
	}
	
	public PaginationItem getPagination(IQueryResult resultSet, int offset) {
		return getPagination(resultSet, offset, itemPerPage);
	}
	
	public int getPages() {
		return pages;
	}

	public void setPages(int pages) {
		this.pages = pages;
	}

	public int getItemPerPage() {
		return itemPerPage;
	}

	public void setItemPerPage(int itemPerPage) {
		this.itemPerPage = itemPerPage;
	}
	
	public class PaginationItem {
		List<PageItem> pages;
		PageItem current;
		PageItem next;
		PageItem prev;
		int itemTotal;
		
		public List<PageItem> getPages() {
			return pages;
		}
		public PageItem getCurrent() {
			return current;
		}
		public PageItem getNext() {
			return next;
		}
		public int getItemTotal() {
			return itemTotal;
		}
		public PageItem getPrev() {
			return prev;
		}
	}
	
	public class PageItem {
		private int page;
		private int offset;
		private int itemBegin;
		private int itemEnd;

		private boolean isCurrent;
		
		public int getPage() {
			return page;
		}
		public int getOffset() {
			return offset;
		}
		public boolean isCurrent() {
			return isCurrent;
		}
		public int getItemBegin() {
			return itemBegin;
		}
		public int getItemEnd() {
			return itemEnd;
		}
	}
}
