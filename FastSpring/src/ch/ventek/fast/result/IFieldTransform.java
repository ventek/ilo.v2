package ch.ventek.fast.result;


public interface IFieldTransform {
	
	abstract Object process(Object s);
}
