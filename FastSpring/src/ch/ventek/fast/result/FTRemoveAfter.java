package ch.ventek.fast.result;


public class FTRemoveAfter implements IFieldTransform {

	private String after;

	public Object process(Object obj) {
		if (obj == null)
			return obj;
		else if (obj instanceof String) {
			String s = (String) obj;
			int pos = s.indexOf(after);
			
			if (pos > -1)
				s = s.substring(0, pos);
			
			return s;
		}
		else
			return obj;
	}

	public void setAfter(String after) {
		this.after = after;
	}


}
