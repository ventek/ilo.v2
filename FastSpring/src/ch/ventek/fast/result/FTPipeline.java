package ch.ventek.fast.result;

import java.util.List;


public class FTPipeline implements IFieldTransform {

	List<IFieldTransform> fieldTransformList;
	
	public Object process(Object obj) {
		for (IFieldTransform ft : fieldTransformList)
			obj = ft.process(obj);
		return obj;
	}

	public void setFieldTransformList(List<IFieldTransform> fieldTransformList) {
		this.fieldTransformList = fieldTransformList;
	}

}
