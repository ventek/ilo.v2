package ch.ventek.fast.result;

import java.util.ArrayList;
import java.util.List;

import ch.ventek.fast.navigation.INavigationBuilder;
import ch.ventek.fast.navigation.Modifier;

/**
 * 
 *	<c:forEach var="breadcrumb" items="${results.breadcrumb}">
 *		<div>
 *			<c:url value="result.htm" var="url">
 *				<c:param name="searchWhat" value="${httpQuery.searchWhat}"/>
 *				<c:param name="searchIn" value="${httpQuery.searchIn}"/>
 *				<c:param name="navigators" value="${breadcrumb.refinestr}"/>
 *			</c:url>
 *			<strong>${breadcrumb.name}</strong>
 *			<a href="${url}"><span>Remove</span></a>
 *		</div>
 *	</c:forEach>
 * 
 * 
 * @author Ake Tangkananond
 *
 */
public class ResultBreadcrumb {
	
	public List<BreadcrumbItem> getBreadcrumb(String navstr, INavigationBuilder builder) {
		List<BreadcrumbItem> breadcrumb = new ArrayList<BreadcrumbItem>();
		List<Modifier> modifiers = builder.extractModifiers(navstr);
		
		for (Modifier modifier : modifiers) {
			List<Modifier> _modifiers = new ArrayList<Modifier>(modifiers.size());
			_modifiers.addAll(modifiers);
			_modifiers.remove(modifier);
			
			BreadcrumbItem breadcrumbItem = new BreadcrumbItem();
			breadcrumbItem.modifier = modifier;
			breadcrumbItem.name = modifier.modName;
			breadcrumbItem.refinestr = builder.packModifiers(_modifiers);
			breadcrumb.add(breadcrumbItem);
		}
		
		return breadcrumb;
	}
	
	public class BreadcrumbItem {
		public Modifier modifier;
		
		private String name;
		private String refinestr;
		
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getRefinestr() {
			return refinestr;
		}
		public void setRefinestr(String refinestr) {
			this.refinestr = refinestr;
		}
		public Modifier getModifier() {
			return modifier;
		}
		@Override
		public String toString() {
			return name;
		}
	}
}
