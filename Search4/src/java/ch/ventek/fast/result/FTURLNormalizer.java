package ch.ventek.fast.result;

import org.apache.log4j.Logger;

public class FTURLNormalizer implements IFieldTransform {	
	private static Logger logger = Logger.getLogger(FTURLNormalizer.class);
	
	public Object process(Object s) {
		if (! (s instanceof String) )
			return s;
		
		String url = (String) s;
		
		if ( url.startsWith("http://") ) {
			return url;
		}
		else if ( url.startsWith("www") ) {
			return "http://" + url;
		}
		else
			return "http://" + url;
	}
}
