package ch.ventek.locale;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.ModelAndView;

public class RequestLocaleHolder implements HandlerInterceptor {
	private static Logger logger = Logger.getLogger(RequestLocaleHolder.class);
	
	public static final Locale en_US = new Locale("EN", "US");
	public static final Locale es_ES = new Locale("ES", "ES");
	public static final Locale fr_FR = new Locale("FR", "FR");
	
	private static RequestLocaleHolder instance;
	private static final ThreadLocal<Locale> threadLocale = new ThreadLocal<Locale>();
	
	private LocaleResolver localeResolver;
	
	public static synchronized RequestLocaleHolder getInstance() {
		if ( instance == null )
			instance  = new RequestLocaleHolder();
		return instance;
	}
	
	public static String i18nMuxString(Locale locale, String en, String es, String fr) {
		if ( es == null || es.trim().length() == 0 )
			es = en;
		
		if ( fr == null || fr.trim().length() == 0 )
			fr = en;
		
		if ( RequestLocaleHolder.en_US.equals(locale) )
			return en;
		else if ( RequestLocaleHolder.es_ES.equals(locale) )
			return es;
		else if ( RequestLocaleHolder.fr_FR.equals(locale) )
			return fr;
		else {
			logger.warn("Unexpected locale, default is used");
			return en;
		}
	}
	
	private RequestLocaleHolder() {}
	
	public Object clone() throws CloneNotSupportedException {
		throw new CloneNotSupportedException();
	}
	
	public static Locale getLocale() throws IllegalStateException {
		Locale locale = RequestLocaleHolder.threadLocale.get();

		return locale;
	}
	
	public Locale getCurrentLocale() throws IllegalStateException {
		return RequestLocaleHolder.getLocale();
	}
	
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
		RequestLocaleHolder.threadLocale.set(null);
	}

	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
		
	}

	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		Locale locale = localeResolver.resolveLocale(request);
		RequestLocaleHolder.threadLocale.set(locale);
		
		return true;
	}

	public LocaleResolver getLocaleResolver() {
		return localeResolver;
	}

	public void setLocaleResolver(LocaleResolver localeResolver) {
		this.localeResolver = localeResolver;
	}
	
}
