package ch.ventek.ilo.domain;

import java.util.Arrays;


public class AdvSearchHttpRequest extends SearchHttpRequest {
	private String searchWhatNot;
	
	private String searchMode;
	private String resultPerPage;
	private String dateFrom;
	private String dateTo;
	
	private String [] language;
	private String [] type;
	private String [] format;
	private String [] theme;
	private String [] region;
	
	private String parsedSearchMode;
	private String parsedResultPerPage;
	private String parsedDateFrom;
	private String parsedDateTo;
	private String [] parsedLanguage;
	private String [] parsedType;
	private String [] parsedFormat;
	private String [] parsedTheme;
	private String [] parsedRegion;
	
	private String rmDateFrom;
	private String rmDateTo;
	private String rmLanguage;
	private String rmType;
	private String rmFormat;
	private String rmTheme;
	private String rmRegion;
	
	private boolean parsed = false;
	
	public AdvSearchHttpRequest() {
		super();
	}

	public String getSearchWhatNot() {
		return searchWhatNot;
	}

	public void setSearchWhatNot(String searchWhatNot) {
		this.searchWhatNot = searchWhatNot;
	}

	public String[] getLanguage() {
		return language;
	}

	public void setLanguage(String[] language) {
		this.language = language;
	}

	public String[] getType() {
		return type;
	}

	public void setType(String[] type) {
		this.type = type;
	}

	public String[] getFormat() {
		return format;
	}

	public void setFormat(String[] format) {
		this.format = format;
	}

	public String[] getTheme() {
		return theme;
	}

	public void setTheme(String[] theme) {
		this.theme = theme;
	}

	public boolean isParsed() {
		return parsed;
	}

	public void setParsed(boolean parsed) {
		this.parsed = parsed;
	}

	public String getResultPerPage() {
		return resultPerPage;
	}

	public void setResultPerPage(String resultPerPage) {
		this.resultPerPage = resultPerPage;
	}
	
	public String toString() {
		StringBuffer buf = new StringBuffer();
		buf.append( super.toString() );
		if ( parsed )
			buf.append( "\n - parsed - \n");
		else
			buf.append( "\n - unparsed - \n");
		
		buf.append( "searchWhatNot: " + searchWhatNot + "\n" );
		buf.append( "searchMode: " + searchMode + " - " + parsedSearchMode + "\n" );
		buf.append( "result per page: " + resultPerPage + " - " + parsedResultPerPage + "\n" );
		buf.append( "datefrom: " + dateFrom + " - " + parsedDateFrom + "\n" );
		buf.append( "dateto: " + dateTo + " - " + parsedDateTo + "\n" );
		buf.append( "language: " + Arrays.toString(language) + " - " + parsedLanguage + "\n" );
		buf.append( "type: " + Arrays.toString(type) + " - " + parsedType + "\n" );
		buf.append( "format: " + Arrays.toString(format) + " - " + parsedFormat + "\n" );
		buf.append( "theme: " + Arrays.toString(theme) + " - " + parsedTheme + "\n" );
		
		return buf.toString();
	}

	public String getDateFrom() {
		return dateFrom;
	}

	public void setDateFrom(String dateFrom) {
		this.dateFrom = dateFrom;
	}

	public String getDateTo() {
		return dateTo;
	}

	public void setDateTo(String dateTo) {
		this.dateTo = dateTo;
	}

	public String getSearchMode() {
		return searchMode;
	}

	public void setSearchMode(String searchMode) {
		this.searchMode = searchMode;
	}

	public String getParsedSearchMode() {
		return parsedSearchMode;
	}

	public void setParsedSearchMode(String parsedSearchMode) {
		this.parsedSearchMode = parsedSearchMode;
	}

	public String getParsedResultPerPage() {
		return parsedResultPerPage;
	}

	public void setParsedResultPerPage(String parsedResultPerPage) {
		this.parsedResultPerPage = parsedResultPerPage;
	}

	public String getParsedDateFrom() {
		return parsedDateFrom;
	}

	public void setParsedDateFrom(String parsedDateFrom) {
		this.parsedDateFrom = parsedDateFrom;
	}

	public String getParsedDateTo() {
		return parsedDateTo;
	}

	public void setParsedDateTo(String parsedDateTo) {
		this.parsedDateTo = parsedDateTo;
	}

	public String[] getParsedLanguage() {
		return parsedLanguage;
	}

	public void setParsedLanguage(String[] parsedLanguage) {
		this.parsedLanguage = parsedLanguage;
	}

	public String[] getParsedType() {
		return parsedType;
	}

	public void setParsedType(String[] parsedType) {
		this.parsedType = parsedType;
	}

	public String[] getParsedFormat() {
		return parsedFormat;
	}

	public void setParsedFormat(String[] parsedFormat) {
		this.parsedFormat = parsedFormat;
	}

	public String[] getParsedTheme() {
		return parsedTheme;
	}

	public void setParsedTheme(String[] parsedTheme) {
		this.parsedTheme = parsedTheme;
	}

	public String getRmDateFrom() {
		return rmDateFrom;
	}

	public void setRmDateFrom(String rmDateFrom) {
		this.rmDateFrom = rmDateFrom;
	}

	public String getRmDateTo() {
		return rmDateTo;
	}

	public void setRmDateTo(String rmDateTo) {
		this.rmDateTo = rmDateTo;
	}

	public String getRmLanguage() {
		return rmLanguage;
	}

	public void setRmLanguage(String rmLanguage) {
		this.rmLanguage = rmLanguage;
	}

	public String getRmType() {
		return rmType;
	}

	public void setRmType(String rmType) {
		this.rmType = rmType;
	}

	public String getRmFormat() {
		return rmFormat;
	}

	public void setRmFormat(String rmFormat) {
		this.rmFormat = rmFormat;
	}

	public String getRmTheme() {
		return rmTheme;
	}

	public void setRmTheme(String rmTheme) {
		this.rmTheme = rmTheme;
	}

	public String[] getRegion() {
		return region;
	}

	public void setRegion(String[] region) {
		this.region = region;
	}

	public String[] getParsedRegion() {
		return parsedRegion;
	}

	public void setParsedRegion(String[] parsedRegion) {
		this.parsedRegion = parsedRegion;
	}

	public String getRmRegion() {
		return rmRegion;
	}

	public void setRmRegion(String rmRegion) {
		this.rmRegion = rmRegion;
	}
	
}
