package ch.ventek.ilo.domain;

import java.text.SimpleDateFormat;

public class SearchHttpRequest {
	public static final SimpleDateFormat advSearchDateFormat = new SimpleDateFormat("dd-MM-yyyy");
	public static final SimpleDateFormat fastDateFormat = new SimpleDateFormat("yyyy-MM-dd");
	public static final String SORTBY_DATE = "docdatetime";
	public static final String SORTBY_RELEVANCE = "default";
	
	public static final String ACTION_FORM = "form";
	public static final String ACTION_SEARCH = "search";
	public static final String ACTION_RSS = "rss";
	
	public static final String COLLECTION_ALL = "*";
	public static final String COLLECTION_LBD = "ILOLaborDoc";
	
	private String action;
	private int lastDay;
	private String searchURI; /* use in form */
	private String searchMethod; /* use in form */

	private String collection;
	
	private String locale;
	private String searchWhat;
    private String searchLanguage;
	private String navigators;
	private String sortby;
	private int hits;
	private int offset;
		
	public SearchHttpRequest() {
		searchWhat = "";
		navigators = "";
        searchLanguage = "";

		hits = 10;
		offset = 0;
		sortby = SORTBY_RELEVANCE;
		
		collection = "";
	}
	
	public String getSearchWhat() {
		return searchWhat;
	}
	public void setSearchWhat(String searchWhat) {
		this.searchWhat = searchWhat;
	}
	public String getNavigators() {
		return navigators;
	}
	public void setNavigators(String navigators) {
		this.navigators = navigators;
	}
	public int getHits() {
		return hits;
	}
	public void setHits(int hits) {
		this.hits = hits;
	}
	public int getOffset() {
		return offset;
	}
	public void setOffset(int offset) {
		this.offset = offset;
	}
	public String toString() {
		StringBuffer buf = new StringBuffer();
		buf.append("[" + this.searchWhat + "]");
		if ( this.navigators != null && this.navigators.trim().length() > 0)
			buf.append("++");
		return buf.toString();
	}

	public String getLocale() {
		return locale;
	}

	public void setLocale(String locale) {
		this.locale = locale;
	}

	public String getSortby() {
		return sortby;
	}

	public void setSortby(String sortby) {
		this.sortby = sortby;
	}

	public String getSearchURI() {
		return searchURI;
	}

	public void setSearchURI(String searchURI) {
		this.searchURI = searchURI;
	}

	public String getSearchMethod() {
		return searchMethod;
	}

	public void setSearchMethod(String searchMethod) {
		this.searchMethod = searchMethod;
	}	

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public int getLastDay() {
		return lastDay;
	}

	public void setLastDay(int lastDay) {
		this.lastDay = lastDay;
	}

	public String getCollection() {
		return collection;
	}

	public void setCollection(String collection) {
		this.collection = collection;
	}

    public String getSearchLanguage() {
        return searchLanguage;
    }

    public void setSearchLanguage(String searchLanguage) {
        this.searchLanguage = searchLanguage;
    }

    public boolean isPhrase() {
		return searchWhat != null && searchWhat.charAt(0) == '\"' && searchWhat.charAt(searchWhat.length() - 1) == '\"';
	}
	
}
