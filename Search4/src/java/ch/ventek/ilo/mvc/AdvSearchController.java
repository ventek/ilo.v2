package ch.ventek.ilo.mvc;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

import ch.ventek.fast.util.FASTUtils;
import ch.ventek.ilo.domain.AdvSearchHttpRequest;
import ch.ventek.ilo.domain.SearchHttpRequest;

public class AdvSearchController extends SearchController {
	private static Logger logger = Logger.getLogger(AdvSearchController.class);
	
	private Map<String, String> advSearchMapSearchMode;
	private Map<String, String> advSearchMapResultPerPage;
	private Map<String, String> advSearchMapLanguage;
	private Map<String, String> advSearchMapType;
	private Map<String, String> advSearchMapFormat;
	private Map<String, String> advSearchMapTheme;
	private Map<String, String> advSearchMapRegion;
	
	@Override
	protected Object formBackingObject(HttpServletRequest request) throws Exception {
		AdvSearchHttpRequest httpQuery = (AdvSearchHttpRequest) super.formBackingObject(request);
		httpQuery.setSearchURI("/advsearch.do");
		httpQuery.setSearchMethod("post");
		httpQuery.setParsed(false);
		return httpQuery;
	}
	
	protected boolean isFormSubmission(HttpServletRequest request) {
		return request.getParameterMap().size() > 0;
	}
	
	@Override
	protected ModelAndView handleRequestInternal(HttpServletRequest request, HttpServletResponse response) throws Exception {
		if ( AdvSearchHttpRequest.ACTION_SEARCH.equals(request.getParameter("action")) ) {
			HttpSession session = request.getSession(false);
			if (session != null) {
				String formAttrName = getFormSessionAttributeName(request);
				session.removeAttribute(formAttrName);
			}
		}
				
		return super.handleRequestInternal(request, response);
	}

	@Override
	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors) throws Exception {
		super.onBindAndValidate(request, command, errors);
		
		int i;
		ArrayList<String> list = new ArrayList<String>();
		String [] sstr;
		Date date;
		AdvSearchHttpRequest httpQuery = (AdvSearchHttpRequest) command;
		
		if ( httpQuery.getRmDateFrom() != null ) {
			httpQuery.setDateFrom(null);
			httpQuery.setParsed(false);
		}
		if ( httpQuery.getRmDateTo() != null ) {
			httpQuery.setDateTo(null);
			httpQuery.setParsed(false);
		}
		if ( httpQuery.getRmLanguage() != null && httpQuery.getLanguage() != null ) {
			i = 0;
			list.clear();
			while ( i < httpQuery.getLanguage().length && !httpQuery.getRmLanguage().equals(httpQuery.getLanguage()[i]) ) {
				list.add(httpQuery.getLanguage()[i]);
				i++;
			}
			i++;
			while ( i < httpQuery.getLanguage().length ) {
				list.add(httpQuery.getLanguage()[i]);
				i++;
			}
			sstr = new String[list.size()];
			sstr = list.toArray(sstr);
			httpQuery.setLanguage(sstr);
			httpQuery.setParsed(false);
		}
		if ( httpQuery.getRmType() != null && httpQuery.getType() != null ) {
			i = 0;
			list.clear();
			while ( i < httpQuery.getType().length && !httpQuery.getRmType().equals(httpQuery.getType()[i]) ) {
				list.add(httpQuery.getType()[i]);
				i++;
			}
			i++;
			while ( i < httpQuery.getType().length ) {
				list.add(httpQuery.getType()[i]);
				i++;
			}
			sstr = new String[list.size()];
			sstr = list.toArray(sstr);
			httpQuery.setType(sstr);
			httpQuery.setParsed(false);
		}
		if ( httpQuery.getRmFormat() != null && httpQuery.getFormat() != null ) {
			i = 0;
			list.clear();
			while ( i < httpQuery.getFormat().length && !httpQuery.getRmFormat().equals(httpQuery.getFormat()[i]) ) {
				list.add(httpQuery.getFormat()[i]);
				i++;
			}
			i++;
			while ( i < httpQuery.getFormat().length ) {
				list.add(httpQuery.getFormat()[i]);
				i++;
			}
			sstr = new String[list.size()];
			sstr = list.toArray(sstr);
			httpQuery.setFormat(sstr);
			httpQuery.setParsed(false);
		}
		if ( httpQuery.getRmTheme() != null && httpQuery.getTheme() != null ) {
			i = 0;
			list.clear();
			while ( i < httpQuery.getTheme().length && !httpQuery.getRmTheme().equals(httpQuery.getTheme()[i]) ) {
				list.add(httpQuery.getTheme()[i]);
				i++;
			}
			i++;
			while ( i < httpQuery.getTheme().length ) {
				list.add(httpQuery.getTheme()[i]);
				i++;
			}
			sstr = new String[list.size()];
			sstr = list.toArray(sstr);
			httpQuery.setTheme(sstr);
			httpQuery.setParsed(false);
		}
		if ( httpQuery.getRmRegion() != null && httpQuery.getRegion() != null ) {
			i = 0;
			list.clear();
			while ( i < httpQuery.getRegion().length && !httpQuery.getRmRegion().equals(httpQuery.getRegion()[i]) ) {
				list.add(httpQuery.getRegion()[i]);
				i++;
			}
			i++;
			while ( i < httpQuery.getRegion().length ) {
				list.add(httpQuery.getRegion()[i]);
				i++;
			}
			sstr = new String[list.size()];
			sstr = list.toArray(sstr);
			httpQuery.setRegion(sstr);
			httpQuery.setParsed(false);
		}
		
		if ( ! httpQuery.isParsed() ) {
			if ( httpQuery.getSearchMode() != null && advSearchMapSearchMode.containsKey(httpQuery.getSearchMode()) )
				httpQuery.setParsedSearchMode( advSearchMapSearchMode.get(httpQuery.getSearchMode()) );
			else
				httpQuery.setParsedSearchMode( advSearchMapSearchMode.entrySet().iterator().next().getValue() );

			if ( httpQuery.getResultPerPage() != null && advSearchMapResultPerPage.containsKey(httpQuery.getResultPerPage()) )
				httpQuery.setParsedResultPerPage( advSearchMapResultPerPage.get(httpQuery.getResultPerPage()) );
			else
				httpQuery.setParsedResultPerPage( advSearchMapResultPerPage.entrySet().iterator().next().getValue() );
			httpQuery.setHits( Integer.parseInt(httpQuery.getParsedResultPerPage()) );
			
			if ( httpQuery.getDateFrom() != null ) {
				try {
					date = AdvSearchHttpRequest.advSearchDateFormat.parse( httpQuery.getDateFrom() );
					httpQuery.setParsedDateFrom( AdvSearchHttpRequest.fastDateFormat.format(date) );
				}
				catch (ParseException ex) {
					httpQuery.setDateFrom( null );
					httpQuery.setParsedDateFrom( null );
				}
			}
			if ( httpQuery.getDateTo() != null ) {
				try {
					date = AdvSearchHttpRequest.advSearchDateFormat.parse( httpQuery.getDateTo() );
					httpQuery.setParsedDateTo( AdvSearchHttpRequest.fastDateFormat.format(date) );
				}
				catch (ParseException ex) {
					httpQuery.setDateTo( null );
					httpQuery.setParsedDateTo( null );
				}
			}
			
			if ( httpQuery.getParsedDateFrom() != null && httpQuery.getParsedDateTo() != null ) {
				try {
					Date dfrom = AdvSearchHttpRequest.advSearchDateFormat.parse( httpQuery.getParsedDateFrom() );
					Date dto = AdvSearchHttpRequest.advSearchDateFormat.parse( httpQuery.getParsedDateTo() );
					if ( dfrom.after(dto) )
						throw new Exception();
				}
				catch (Exception ex) {
					httpQuery.setDateFrom(null);
					httpQuery.setDateTo(null);
					httpQuery.setParsedDateFrom(null);
					httpQuery.setParsedDateTo(null);
					errors.rejectValue("dateFrom", "error.date.invalidrange");
				}
			}
			
			if ( httpQuery.getLanguage() != null ) {
				list.clear();
				for ( i = 0; i < httpQuery.getLanguage().length; i++ ) {
					if ( advSearchMapLanguage.containsKey(httpQuery.getLanguage()[i]) ) {
						list.add(FASTUtils.escapeFQL(advSearchMapLanguage.get(httpQuery.getLanguage()[i])));
					}
				}
				sstr = new String[list.size()];
				sstr = list.toArray(sstr);
				httpQuery.setParsedLanguage( sstr );
			}
			
			if ( httpQuery.getType() != null ) {
				list.clear();
				for ( i = 0; i < httpQuery.getType().length; i++ ) {
					if ( advSearchMapType.containsKey(httpQuery.getType()[i]) ) {
						list.add(FASTUtils.escapeFQL(advSearchMapType.get(httpQuery.getType()[i])));
					}
				}
				sstr = new String[list.size()];
				sstr = list.toArray(sstr);
				httpQuery.setParsedType( sstr );
			}
			
			if ( httpQuery.getFormat() != null ) {
				list.clear();
				for ( i = 0; i < httpQuery.getFormat().length; i++ ) {
					if ( advSearchMapFormat.containsKey(httpQuery.getFormat()[i]) ) {
						list.add(FASTUtils.escapeFQL(advSearchMapFormat.get(httpQuery.getFormat()[i])));
					}
				}
				sstr = new String[list.size()];
				sstr = list.toArray(sstr);
				httpQuery.setParsedFormat( sstr );
			}
			
			if ( httpQuery.getTheme() != null ) {
				list.clear();
				for ( i = 0; i < httpQuery.getTheme().length; i++ ) {
					if ( advSearchMapTheme.containsKey(httpQuery.getTheme()[i]) ) {
						list.add(FASTUtils.escapeFQL(advSearchMapTheme.get(httpQuery.getTheme()[i])));
					}
				}
				sstr = new String[list.size()];
				sstr = list.toArray(sstr);
				httpQuery.setParsedTheme( sstr );
			}
			
			if ( httpQuery.getRegion() != null ) {
				list.clear();
				for ( i = 0; i < httpQuery.getRegion().length; i++ ) {
					if ( advSearchMapRegion.containsKey(httpQuery.getRegion()[i]) ) {
						list.add(FASTUtils.escapeFQL(advSearchMapRegion.get(httpQuery.getRegion()[i])));
					}
				}
				sstr = new String[list.size()];
				sstr = list.toArray(sstr);
				httpQuery.setParsedRegion( sstr );
			}
			
			httpQuery.setParsed(true);
		}
		
		logger.info( httpQuery );
	}

	protected ModelAndView processFormSubmission(HttpServletRequest request, HttpServletResponse response, Object obj, BindException errors) throws Exception {
		AdvSearchHttpRequest httpQuery = (AdvSearchHttpRequest) obj;
		Map<String, Object> controlMap = new HashMap<String, Object>();
		String [] sstr;

		if ( SearchHttpRequest.ACTION_FORM.equals(httpQuery.getAction()) ) {
			return showForm(request, response, errors);
		}
		else if ( errors.hasErrors() ) {
			return showForm(request, response, errors);
		}
		
		HashMap<String, String> query = new HashMap<String, String>();
		query.put("{0}", FASTUtils.purifyQuery(httpQuery.getSearchWhat()));
		query.put("{1}", httpQuery.getParsedSearchMode() );
		
		if ( httpQuery.getLastDay() > 0 ) {
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.DATE, -httpQuery.getLastDay());
			query.put("{21}", SearchHttpRequest.fastDateFormat.format(cal.getTime()));
		}
		else {
			query.put("{21}", "min");
		}
		query.put("{22}", "max");
		
		sstr = httpQuery.getParsedLanguage();
		if ( sstr != null && sstr.length > 0 )
			query.put("{2}", "\"" + StringUtils.arrayToDelimitedString(sstr, "\",\"") + "\"" );
		else
			query.put("{2}", "\"\"");
		
		sstr = httpQuery.getParsedType();
		if ( sstr != null && sstr.length > 0 )
			query.put("{3}", "\"" + StringUtils.arrayToDelimitedString(sstr, "\",\"") + "\"" );
		else
			query.put("{3}", "\"\"");
		
		sstr = httpQuery.getParsedFormat();
		if ( sstr != null && sstr.length > 0 )
			query.put("{4}", "\"" + StringUtils.arrayToDelimitedString(sstr, "\",\"") + "\"" );
		else
			query.put("{4}", "\"\"");
		
		sstr = httpQuery.getParsedTheme();
		if ( sstr != null && sstr.length > 0 )
			query.put("{5}", "\"" + StringUtils.arrayToDelimitedString(sstr, "\",\"") + "\"" );
		else
			query.put("{5}", "\"\"");
		
		sstr = httpQuery.getParsedRegion();
		if ( sstr != null && sstr.length > 0 )
			query.put("{6}", "\"" + StringUtils.arrayToDelimitedString(sstr, "\",\"") + "\"" );
		else
			query.put("{6}", "\"\"");
		
		if ( httpQuery.getParsedDateFrom() != null )
			query.put("{7}", httpQuery.getParsedDateFrom());
		else
			query.put("{7}", "min");
		
		if ( httpQuery.getParsedDateTo() != null )
			query.put("{8}", httpQuery.getParsedDateTo());
		else
			query.put("{8}", "max");
		
		if ( httpQuery.getSearchWhatNot() != null )
			query.put("{11}", httpQuery.getSearchWhatNot());
		
		query.put("{99}", "*"); 
		httpQuery.setAction( SearchHttpRequest.ACTION_SEARCH );
		Map<String, Object> results = getSearchResults(query, searchILO, resultILODocuments, resultILONavigation, httpQuery);
		controlMap.put("results", results);

		return showForm(request, errors, "search", controlMap);
	}

	@Override
	protected ModelAndView showForm(HttpServletRequest request, HttpServletResponse response, BindException be) throws Exception {
		AdvSearchHttpRequest httpQuery = (AdvSearchHttpRequest) be.getTarget();
		httpQuery.setAction( SearchHttpRequest.ACTION_FORM );
		return showForm(request, be, "advsearch", null);
	}

	public Map<String, String> getAdvSearchMapResultPerPage() {
		return advSearchMapResultPerPage;
	}

	public void setAdvSearchMapResultPerPage(
			Map<String, String> advSearchMapResultPerPage) {
		this.advSearchMapResultPerPage = advSearchMapResultPerPage;
	}

	public Map<String, String> getAdvSearchMapLanguage() {
		return advSearchMapLanguage;
	}

	public void setAdvSearchMapLanguage(Map<String, String> advSearchMapLanguage) {
		this.advSearchMapLanguage = advSearchMapLanguage;
	}

	public Map<String, String> getAdvSearchMapType() {
		return advSearchMapType;
	}

	public void setAdvSearchMapType(Map<String, String> advSearchMapType) {
		this.advSearchMapType = advSearchMapType;
	}

	public Map<String, String> getAdvSearchMapFormat() {
		return advSearchMapFormat;
	}

	public void setAdvSearchMapFormat(Map<String, String> advSearchMapFormat) {
		this.advSearchMapFormat = advSearchMapFormat;
	}

	public Map<String, String> getAdvSearchMapTheme() {
		return advSearchMapTheme;
	}

	public void setAdvSearchMapTheme(Map<String, String> advSearchMapTheme) {
		this.advSearchMapTheme = advSearchMapTheme;
	}

	public Map<String, String> getAdvSearchMapSearchMode() {
		return advSearchMapSearchMode;
	}

	public void setAdvSearchMapSearchMode(Map<String, String> advSearchMapSearchMode) {
		this.advSearchMapSearchMode = advSearchMapSearchMode;
	}

	public Map<String, String> getAdvSearchMapRegion() {
		return advSearchMapRegion;
	}

	public void setAdvSearchMapRegion(Map<String, String> advSearchMapRegion) {
		this.advSearchMapRegion = advSearchMapRegion;
	}	
}
