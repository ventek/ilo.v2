package ch.ventek.ilo.mvc;

import ch.ventek.fast.navigation.INavigationBuilder;
import ch.ventek.fast.navigation.Modifier;
import ch.ventek.fast.result.ResultBreadcrumb;
import ch.ventek.fast.result.ResultDocuments;
import ch.ventek.fast.result.ResultNavigation;
import ch.ventek.fast.result.ResultNavigation.ModifierItem;
import ch.ventek.fast.result.ResultPagination;
import ch.ventek.fast.search.FASTSearch;
import ch.ventek.fast.util.FASTUtils;
import ch.ventek.ilo.domain.SearchHttpRequest;
import ch.ventek.locale.RequestLocaleHolder;
import com.fastsearch.esp.search.navigation.IAdjustment;
import com.fastsearch.esp.search.navigation.IAdjustmentGroup;
import com.fastsearch.esp.search.navigation.INavigation;
import com.fastsearch.esp.search.query.BaseParameter;
import com.fastsearch.esp.search.query.SearchParameter;
import com.fastsearch.esp.search.result.IQueryResult;
import com.fastsearch.esp.search.result.IQueryTransformation;
import com.fastsearch.esp.search.result.IQueryTransformations;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractFormController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.net.URLEncoder;
import java.util.*;

public class SearchController extends AbstractFormController {
	private static Logger logger = Logger.getLogger(SearchController.class);
	
	protected FASTSearch searchILO;
	protected ResultDocuments resultILODocuments;
	protected ResultNavigation resultILONavigation;
	protected ResultNavigation resultILOLBDNavigation;
	
	protected ResultPagination resultPagination;
	protected ResultBreadcrumb resultBreadcrumb;
	protected INavigationBuilder navigationBuilder;
	
	protected int bestBetCutOff;
	
	@Override
	protected Object formBackingObject(HttpServletRequest request) throws Exception {
		SearchHttpRequest httpQuery = (SearchHttpRequest) super.formBackingObject(request);
		httpQuery.setSearchURI("/search.do");
		httpQuery.setSearchMethod("get");
		return httpQuery;
	}
	
	protected boolean isFormSubmission(HttpServletRequest request) {
		return true;
	}
		
	@Override
	protected ModelAndView handleRequestInternal(HttpServletRequest request, HttpServletResponse response) throws Exception {
		if ( "POST".equals(request.getMethod()) && request.getParameter("query") != null && request.getParameter("sitelang") != null ) {
			// deprecated
			// handle search from CMS
			ModelAndView mav = new ModelAndView();
			StringBuffer buf = new StringBuffer();
			buf.append( "redirect:" );
			buf.append( response.encodeRedirectURL("/search.do") );
			buf.append( "?" );
			buf.append( "searchWhat=" + URLEncoder.encode(request.getParameter("query"), "UTF-8") );
			
			String sitelang = request.getParameter("sitelang");
			if ( "en".equals(sitelang) )
				buf.append( "&locale=" + RequestLocaleHolder.en_US );
			else if ( "es".equals(sitelang) )
				buf.append( "&locale=" + RequestLocaleHolder.es_ES );
			else if ( "fr".equals(sitelang) )
				buf.append( "&locale=" + RequestLocaleHolder.fr_FR );
			else
				buf.append( "&locale=" + RequestLocaleHolder.en_US );
			
			mav.setViewName(buf.toString());
			return mav;
		}
		else
			return super.handleRequestInternal(request, response);
	}

	@Override
	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors) throws Exception {
		SearchHttpRequest httpQuery = (SearchHttpRequest) command;

        // TODO: UNCOMMENT THIS FOR PRODUCTION
//		if ( ! SearchHttpRequest.SORTBY_DATE.equals(httpQuery.getSortby()) && ! SearchHttpRequest.SORTBY_RELEVANCE.equals(httpQuery.getSortby()) )
//			httpQuery.setSortby( SearchHttpRequest.SORTBY_RELEVANCE );

        if (StringUtils.isBlank(httpQuery.getSortby()))
            httpQuery.setSortby( SearchHttpRequest.SORTBY_RELEVANCE );
	}
	
	protected ModelAndView processFormSubmission(HttpServletRequest request, HttpServletResponse response, Object obj, BindException errors) throws Exception {
		SearchHttpRequest httpQuery = (SearchHttpRequest) obj;
		Map<String, Object> controlMap = new HashMap<String, Object>();
		
		HashMap<String, String> query = new HashMap<String, String>();
		if (httpQuery.getSearchWhat().trim().length() > 0) {
			query.put("{0}", FASTUtils.purifyQuery(httpQuery.getSearchWhat()));
			query.put("{1}", "simpleall");
			
			if ( httpQuery.getLastDay() > 0 ) {
				Calendar cal = Calendar.getInstance();
				cal.add(Calendar.DATE, -httpQuery.getLastDay());
				query.put("{21}", SearchHttpRequest.fastDateFormat.format(cal.getTime()));
			}
			else {
				query.put("{21}", "min");
			}
			query.put("{22}", "max");
			
			Map<String, Object> results;
			if ( SearchHttpRequest.COLLECTION_LBD.equals(httpQuery.getCollection()) ) {
				query.put("{99}", SearchHttpRequest.COLLECTION_LBD);
				results = getSearchResults(query, searchILO, resultILODocuments, resultILOLBDNavigation, httpQuery);
			}
			else {
				query.put("{99}", SearchHttpRequest.COLLECTION_ALL);
				results = getSearchResults(query, searchILO, resultILODocuments, resultILONavigation, httpQuery);
			}
			 
			controlMap.put("results", results);
		}
		
		if ( SearchHttpRequest.ACTION_RSS.equals( httpQuery.getAction() ) )
			return showForm(request, errors, "search.rss", controlMap);
		else {
			httpQuery.setAction( SearchHttpRequest.ACTION_SEARCH );
			return showForm(request, errors, "search", controlMap);
		}
	}

	@Override
	protected ModelAndView showForm(HttpServletRequest request, HttpServletResponse response, BindException be) throws Exception {
		Object command = getCommand(request);
		
		return processFormSubmission(request, response, command, be);
	}
	
	protected void navRemoveRefined(Map<String, ResultNavigation.NavigatorItem> navigation, List<ResultBreadcrumb.BreadcrumbItem> breadcrumb) {
		for ( ResultBreadcrumb.BreadcrumbItem bcItem : breadcrumb ) {
			ResultNavigation.NavigatorItem navItem = navigation.get( bcItem.getModifier().getNavName() );
			if ( navItem != null ) {
				List<ResultNavigation.ModifierItem> mods = navItem.getModifiers();
				int i;
				
				for ( i = 0; i < mods.size(); i++ ) {
					if ( bcItem.getModifier().equals( mods.get(i).getModifier() ) ) {
						mods.remove(i);
						break;
					}
				}
				
				if ( navItem.getModifiers().isEmpty() ) {
					navigation.remove( bcItem.getModifier().getNavName() );
				}
			}
		}
	}
	
	protected void navSortAlphabetically(Map<String, ResultNavigation.NavigatorItem> navigation, List<String> sortKeys) {
		for ( String sortKey : sortKeys ) {
			ResultNavigation.NavigatorItem navItem = navigation.get( sortKey );
			if ( navItem != null ) {
				List<ResultNavigation.ModifierItem> mods = navItem.getModifiers();
				Collections.sort(mods, new Comparator<ResultNavigation.ModifierItem>() {
					public int compare(ModifierItem arg0, ModifierItem arg1) {
						return ((String)arg0.getName()).compareTo((String) arg1.getName());
					}
				});
			}
		}
	}

	public Map<String, Object> getSearchResults(HashMap<String, String> query, FASTSearch fastSearch, ResultDocuments resultDocuments, ResultNavigation resultNavigation, SearchHttpRequest httpQuery) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		List<SearchParameter> searchParams;
		IQueryResult resultSets;
		Calendar cal;
						
		searchParams = new ArrayList<SearchParameter>();
        if (!SearchHttpRequest.SORTBY_RELEVANCE.equals(httpQuery.getSortby()))
		    searchParams.add(new SearchParameter(BaseParameter.SORT_BY, httpQuery.getSortby()));

        // Comment out these below two lines to disable multilanguage spellchecking
        if (StringUtils.isNotBlank(httpQuery.getSearchLanguage()))
            searchParams.add(new SearchParameter(BaseParameter.LANGUAGE, httpQuery.getSearchLanguage()));
		//searchParams.add(new SearchParameter(BaseParameter.SPELL, true));
//        searchParams.add(new SearchParameter("qtf_teaser:view", "hithighlight"));

		if (httpQuery.isPhrase()) {
			query.put("{1}", "PHRASE");
		}

        // handle language enforcement
        if (StringUtils.isNotEmpty(httpQuery.getSearchLanguage())) {
            String lang = httpQuery.getSearchLanguage();
            boolean addlang = true;

            List<Modifier> modifiers = navigationBuilder.extractModifiers(httpQuery.getNavigators());
            for (Modifier modifier : modifiers) {
                if ("languagesnavigator".equals(modifier.getNavName())) {
                    modifier.setModName(lang);
                    modifier.setModValue(lang);
                    addlang = false;
                    break;
                }
            }

            if (addlang) {
                Modifier modifier = new Modifier();
                modifier.setNavName("languagesnavigator");
                modifier.setNavAttr("language");
                modifier.setModName(lang);
                modifier.setModValue(lang);
                modifiers.add(modifier);
            }

            httpQuery.setNavigators(navigationBuilder.packModifiers(modifiers));
        }
		
		resultSets = fastSearch.search(
				query,
                navigationBuilder.getNavigation(httpQuery.getNavigators()),
				httpQuery.getOffset(), 
				httpQuery.getHits(),
				searchParams
			);

		if (resultSets != null) {
			List<Map<String, Object>> documents = resultDocuments.parseDocuments(resultSets, httpQuery.getOffset(), httpQuery.getHits());
			Map<String, ResultNavigation.NavigatorItem> navigation = resultNavigation.getNavigation(resultSets, httpQuery.getNavigators(), navigationBuilder);
			ResultPagination.PaginationItem pagination = resultPagination.getPagination(resultSets, httpQuery.getOffset(), httpQuery.getHits());
			List<ResultBreadcrumb.BreadcrumbItem> breadcrumb = resultBreadcrumb.getBreadcrumb(httpQuery.getNavigators(), navigationBuilder);
			//navRemoveRefined(navigation, breadcrumb);
			navSortAlphabetically(navigation, Arrays.asList("ilocoveragespatialnavigator", "ilotypenavigator"));
			
			if (httpQuery.getOffset() == 0 && httpQuery.getNavigators().length() == 0) {
				int bbCount = 0;
				try {
					for (Map<String, Object> doc : documents) {
						if (Integer.parseInt((String) doc.get("rank")) > bestBetCutOff) {
							bbCount++;
						}
						else {
							break;
						}
					}
				}
				catch (NumberFormatException e) {}
				
				map.put("bb_docs", documents.subList(0, bbCount));
				map.put("docs", documents.subList(bbCount, documents.size()));
			}
			else {
				map.put("docs", documents);
			}
			map.put("pagination", pagination);
			map.put("navigation", navigation);
			map.put("breadcrumb", breadcrumb);
			
			IQueryTransformations iTransF = resultSets.getQueryTransformations(false);
			String didyoumean = null;
			if (iTransF.getSuggestion(IQueryTransformations.DID_YOU_MEAN) != null) {
				Collection<IQueryTransformation> trans = iTransF.getSuggestions(IQueryTransformations.DID_YOU_MEAN);
				for(IQueryTransformation qt : trans){
					if(qt.getMessageID() == 14) {
						didyoumean = qt.getQuery();
						break;
					}
				}
			}
			map.put("didyoumean", didyoumean);
		}
		
		if (httpQuery.getSearchWhat().contains(" ") && ! httpQuery.isPhrase() && httpQuery.getOffset() == 0 && httpQuery.getNavigators().length() == 0) {
			query.put("{1}", "PHRASE");
			searchParams = new ArrayList<SearchParameter>();
			searchParams.add(new SearchParameter(BaseParameter.NAVIGATION, false));
			searchParams.add(new SearchParameter(BaseParameter.HITS, 0));
			resultSets = fastSearch.search(
					query, 
					navigationBuilder.getNavigation(httpQuery.getNavigators()), 
					httpQuery.getOffset(), 
					httpQuery.getHits(),
					searchParams
				);
			if (resultSets != null && resultSets.getDocCount() > 0)
				map.put("phraseDocCount", resultSets.getDocCount());
		}
		
		/*
		cal = Calendar.getInstance();
		cal.add(Calendar.DATE, -7);
		query.put("{21}", SearchHttpRequest.fastDateFormat.format(cal.getTime()));
		searchParams = new ArrayList<SearchParameter>();
		searchParams.add(new SearchParameter(BaseParameter.NAVIGATION, false));
		searchParams.add(new SearchParameter(BaseParameter.HITS, 0));
		resultSets = fastSearch.search(
				query, 
				navigationBuilder.getNavigation(httpQuery.getNavigators()), 
				httpQuery.getOffset(), 
				httpQuery.getHits(),
				searchParams
			);
		if (resultSets != null)
			map.put("docCount7", resultSets.getDocCount());
		
		cal = Calendar.getInstance();
		cal.add(Calendar.DATE, -30);
		query.put("{21}", SearchHttpRequest.fastDateFormat.format(cal.getTime()));
		searchParams = new ArrayList<SearchParameter>();
		searchParams.add(new SearchParameter(BaseParameter.NAVIGATION, false));
		searchParams.add(new SearchParameter(BaseParameter.HITS, 0));
		resultSets = fastSearch.search(
				query, 
				navigationBuilder.getNavigation(httpQuery.getNavigators()), 
				httpQuery.getOffset(), 
				httpQuery.getHits(),
				searchParams
			);
		if (resultSets != null)
			map.put("docCount30", resultSets.getDocCount());
		
		query.put("{21}", "min");
		searchParams = new ArrayList<SearchParameter>();
		searchParams.add(new SearchParameter(BaseParameter.NAVIGATION, false));
		searchParams.add(new SearchParameter(BaseParameter.HITS, 0));
		resultSets = fastSearch.search(
				query, 
				navigationBuilder.getNavigation(httpQuery.getNavigators()), 
				httpQuery.getOffset(), 
				httpQuery.getHits(),
				searchParams
			);
		if (resultSets != null)
			map.put("docCountAll", resultSets.getDocCount());
		
		query.put("{99}", SearchHttpRequest.COLLECTION_ALL);
		resultSets = fastSearch.search(
				query, 
				navigationBuilder.getNavigation(httpQuery.getNavigators()), 
				httpQuery.getOffset(), 
				httpQuery.getHits(),
				searchParams
			);
		if (resultSets != null)
			map.put("docCountSrcAll", resultSets.getDocCount());
		
		query.put("{99}", SearchHttpRequest.COLLECTION_LBD);
		resultSets = fastSearch.search(
				query, 
				navigationBuilder.getNavigation(httpQuery.getNavigators()), 
				httpQuery.getOffset(), 
				httpQuery.getHits(),
				searchParams
			);
		if (resultSets != null)
			map.put("docCountSrcLBD", resultSets.getDocCount());
		*/
		
		
		return map;
	}

	public FASTSearch getSearchILO() {
		return searchILO;
	}

	public void setSearchILO(FASTSearch searchILO) {
		this.searchILO = searchILO;
	}

	public ResultDocuments getResultILODocuments() {
		return resultILODocuments;
	}

	public void setResultILODocuments(ResultDocuments resultILODocuments) {
		this.resultILODocuments = resultILODocuments;
	}

	public ResultNavigation getResultILONavigation() {
		return resultILONavigation;
	}

	public void setResultILONavigation(ResultNavigation resultILONavigation) {
		this.resultILONavigation = resultILONavigation;
	}

	public ResultPagination getResultPagination() {
		return resultPagination;
	}

	public void setResultPagination(ResultPagination resultPagination) {
		this.resultPagination = resultPagination;
	}

	public ResultBreadcrumb getResultBreadcrumb() {
		return resultBreadcrumb;
	}

	public void setResultBreadcrumb(ResultBreadcrumb resultBreadcrumb) {
		this.resultBreadcrumb = resultBreadcrumb;
	}

	public INavigationBuilder getNavigationBuilder() {
		return navigationBuilder;
	}

	public void setNavigationBuilder(INavigationBuilder navigationBuilder) {
		this.navigationBuilder = navigationBuilder;
	}

	public ResultNavigation getResultILOLBDNavigation() {
		return resultILOLBDNavigation;
	}

	public void setResultILOLBDNavigation(ResultNavigation resultILOLBDNavigation) {
		this.resultILOLBDNavigation = resultILOLBDNavigation;
	}

	public int getBestBetCutOff() {
		return bestBetCutOff;
	}

	public void setBestBetCutOff(int bestBetCutOff) {
		this.bestBetCutOff = bestBetCutOff;
	}
	
	

}
