package ch.ventek.ilo.mvc;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractController;

import ch.ventek.fast.search.FASTAutoSuggest;
import ch.ventek.fast.util.FASTUtils;

import java.util.Arrays;

public class AutoSuggestController extends AbstractController {
    private static Logger logger = Logger.getLogger(AutoSuggestController.class);

	FASTAutoSuggest autoSuggestILOEn;
    FASTAutoSuggest autoSuggestILOEs;
    FASTAutoSuggest autoSuggestILOFr;
	
	@Override
	protected ModelAndView handleRequestInternal(HttpServletRequest request, HttpServletResponse response) throws Exception {
		ModelAndView mav = new ModelAndView();
		String q = FASTUtils.purifyQuery( request.getParameter("q") );
        String lang = FASTUtils.purifyQuery(request.getParameter("lang"));

        if ("en".equals(lang)) {
            mav.addObject("suggestions", autoSuggestILOEn.suggest(q));
        }
        else if ("es".equals(lang)) {
            mav.addObject("suggestions", autoSuggestILOEs.suggest(q));
        }
        else if ("fr".equals(lang)) {
            mav.addObject("suggestions", autoSuggestILOFr.suggest(q));
        }
        else {
            mav.addObject("suggestions", autoSuggestILOEn.suggest(q));
        }

		mav.setViewName("suggest");

		return mav;
	}

    public FASTAutoSuggest getAutoSuggestILOEn() {
        return autoSuggestILOEn;
    }

    public void setAutoSuggestILOEn(FASTAutoSuggest autoSuggestILOEn) {
        this.autoSuggestILOEn = autoSuggestILOEn;
    }

    public FASTAutoSuggest getAutoSuggestILOEs() {
        return autoSuggestILOEs;
    }

    public void setAutoSuggestILOEs(FASTAutoSuggest autoSuggestILOEs) {
        this.autoSuggestILOEs = autoSuggestILOEs;
    }

    public FASTAutoSuggest getAutoSuggestILOFr() {
        return autoSuggestILOFr;
    }

    public void setAutoSuggestILOFr(FASTAutoSuggest autoSuggestILOFr) {
        this.autoSuggestILOFr = autoSuggestILOFr;
    }
}
