package ch.ventek.ilo.mvc;

import java.io.IOException;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.XMLWriter;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractController;

import ch.ventek.fast.result.ResultDocuments;
import ch.ventek.fast.search.FASTSearch;
import ch.ventek.fast.util.FASTUtils;

import com.fastsearch.esp.search.result.IQueryResult;

public class MarcDetailsController extends AbstractController {
	private static Logger logger = Logger.getLogger(MarcDetailsController.class);
	
	protected FASTSearch searchILO;
	protected ResultDocuments resultILODocuments;
	
	@Override
	protected ModelAndView handleRequestInternal(HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		String marcid = (String) request.getParameter("marcid");
		ModelAndView mav = new ModelAndView();
		
		if ( marcid != null && marcid.trim().length() > 0 ) {
			HashMap<String, String> query = new HashMap<String, String>();
			query.put( "{0}", FASTUtils.purifyQuery( marcid ) );
			
			Map<String, Object> results = getSearchResults(query, searchILO, resultILODocuments);			
			mav.setViewName("marcdetails");
			mav.addObject("results", results);
		}
		else {
			mav.setViewName("redirect:" + response.encodeRedirectURL("/search.do" ) );
		}
		
		return mav;
	}

	public Map<String, Object> getSearchResults(HashMap<String, String> query, FASTSearch fastSearch, ResultDocuments resultDocuments) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		IQueryResult resultSets;
		int offset = 0;
		int hits = 1;
		
		String xmlHeader = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n<marc:collection xmlns:marc=\"http://www.loc.gov/MARC21/slim\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.loc.gov/MARC21/slim http://www.loc.gov/standards/marcxml/schema/MARC21slim.xsd\">";
		String xmlFooter = "</marc:collection>";
		resultSets = fastSearch.search(query, offset, hits);

		if (resultSets != null) {
			List<Map<String, Object>> documents = resultDocuments.parseDocuments(resultSets, offset, hits);
			
			for ( Map<String, Object> m : documents ) {
				Document doc;
				try {
					doc = DocumentHelper.parseText( xmlHeader + (String) m.get("xml") + xmlFooter );
					StringWriter sw = new StringWriter();
					OutputFormat format = OutputFormat.createPrettyPrint();
					XMLWriter xw = new XMLWriter(sw, format);
					xw.write(doc);
					m.put("prettyxml", (Object) sw.toString());
				} catch (DocumentException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			
			map.put("docs", documents);
		}
		
		return map;
	}

	public FASTSearch getSearchILO() {
		return searchILO;
	}

	public void setSearchILO(FASTSearch searchILO) {
		this.searchILO = searchILO;
	}

	public ResultDocuments getResultILODocuments() {
		return resultILODocuments;
	}

	public void setResultILODocuments(ResultDocuments resultILODocuments) {
		this.resultILODocuments = resultILODocuments;
	}

	
}
