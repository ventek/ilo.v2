﻿<%@ page language="java" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" session="false" %>
<%@ include file="/WEB-INF/jsp/inc/include.jsp" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<c:url value="/assets/images/templates/favicons/msapplication-tileimage.png"/>">


    <title><fmt:message key="search.title" /></title>


    <link rel="stylesheet" media="all" href="<c:url value="/assets/css/screen.css"/>"/>
    <link rel="stylesheet" media="all" href="<c:url value="/css/search.css"/>"/>
    <link rel="stylesheet" media="all" href="<c:url value="/css/jquery.autocomplete.css"/>"/>
    <link rel="stylesheet" href="<c:url value="/assets/js/fancybox/jquery.fancybox.css"/>" type="text/css" media="screen" />

    <link rel="apple-touch-icon-precomposed" href="<c:url value="/assets/images/templates/favicons/apple-touch-icon-precomposed.png"/>">
    <link rel="icon" href="<c:url value="/assets/images/templates/favicons/favicon.png"/>">
    <link rel="shortcut icon" href="<c:url value="/assets/images/templates/favicons/favicon.ico"/>">


    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script src="<c:url value="/js/jquery.elemsort.js"/>"></script>
    <script src="<c:url value="/js/jquery.autocomplete.js"/>"></script>
    <script>
        WebFontConfig = { fontdeck: { id: '45689' } };

        (function() {
            var wf = document.createElement('script');
            wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
                    '://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
            wf.type = 'text/javascript';
            wf.async = 'true';
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(wf, s);
        })();
    </script>
    <script>
        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-4747079-1']);
        _gaq.push(['_trackPageview']);

        (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true; ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0];	s.parentNode.insertBefore(ga, s);
        })();
    </script>


    <!--[if IE 8]><link rel="stylesheet" media="screen" href="<c:url value="/assets/css/ie8.css"/>"><![endif]-->
    <!--[if lt IE 9]><script src="<c:url value="/assets/js/super-shiv.js"/>"></script><![endif]-->
</head>

<body>
<c:import url="inc/header.jsp"/>

<div style="text-align:center;height:200px;margin-top:180px">
    Opps!!! Something is wrong.<br/>
    Please come back few moment later.
</div>

<c:import url="inc/footer.jsp"/>
</body>
</html>