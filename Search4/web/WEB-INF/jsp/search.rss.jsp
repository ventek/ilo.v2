<?xml version="1.0" encoding="UTF-8"?>
<%@ include file="/WEB-INF/jsp/inc/include.jsp" %>
<%@ page language="java" pageEncoding="UTF-8" contentType="text/xml; charset=UTF-8" %>

<rss version="2.0">
	<channel>
		<title>
            <fmt:message key="search.label.searchbar"/>:
            <c:out value="${fn:toLowerCase(httpRequest.searchWhat)} " />
			<c:if test="${fn:length(results.breadcrumb) > 0}">
                <fmt:message key="search.label.match-criteria" />:
				<c:forEach var="breadcrumb" items="${results.breadcrumb}" varStatus="status">
					<spring:message var="text" code="translate.${breadcrumb.modifier.navName}.${breadcrumb.name}" text="${breadcrumb.name}"/>
					<c:if test="${!status.first}">, </c:if>
					${text}
				</c:forEach>
			</c:if>
		</title>
		<link>http://www.ilo.org</link>
		<description><fmt:message key="header.searchservice"/></description>
		<c:forEach var="doc" items="${results.docs}">
			<item>
				<title><c:out value="${fn:replace(fn:replace(doc.title, '<b>', ''), '</b>', '')}"/></title>
				<link><c:url value="${doc.url}"/></link>
				<description><c:out value="${doc.body}" /></description>
				<pubDate><fmt:formatDate value="${doc.docdatetime}" pattern="EEE, d MMM yyyy HH:mm:ss Z"/></pubDate>
				<c:if test="${fn:length(doc.attachment) > 0}">
					<source url="<c:url value="${doc.url}"/>">PDF</source>					
				</c:if>
			</item>
		</c:forEach>
	</channel>
</rss>