﻿<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ include file="/WEB-INF/jsp/inc/include.jsp" %>
<%@ page language="java" pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en" dir="ltr">
<head>
	<title><fmt:message key="search.title" /></title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	
	<script src="<c:url value="/js/jquery.js" />" type="text/javascript"></script>
	<script src="<c:url value="/js/jquery.autocomplete.js" />" type="text/javascript"></script>
	<link rel="stylesheet" type="text/css" href="<c:url value="/css/styles.css" />" media="all" />
	<link rel="stylesheet" type="text/css" href="<c:url value="/css/header.css" />" media="all" />
	<link rel="stylesheet" type="text/css" href="<c:url value="/css/footer.css" />" media="all" />
	<link rel="stylesheet" type="text/css" href="<c:url value="/css/results.css" />" media="all" />
	<link rel="stylesheet" type="text/css" href="<c:url value="/css/jquery.autocomplete.css" />" media="all" />
	
	<style type="text/css">
		.marcdetails dl {
			margin: 0 30px;
		}
		
		.marcdetails dt {
			margin-top: 20px;
			font-weight: bold;
		}
		
		.marcdetails pre.xml {
			width: 780px;
			height: 500px;
			overflow: auto;
			border: solid 1px #d0d0d0;
			
		}
	</style>
</head>

<body>
	<div id="divPrincipal">
		<c:import url="inc/header.jsp"/>
		
		<div id="divContainer" class="marcdetails">
			<h2>MARC details</h2>
			<c:set var="doc" value="${results.docs[0]}"/>
			<c:set var="_prettyxml" value="${fn:replace(doc.prettyxml, '<marc:', '<')}"/>
			<c:set var="_prettyxml" value="${fn:replace(_prettyxml, '</marc:', '</')}"/>
			<x:parse var="pxml" doc="${_prettyxml}" />
			
			<dl>
				<dt>Title</dt>
				<dd>${doc.title}</dd>
				<dt>Abstract</dt>
				<dd>${(fn:length(doc.abstract) > 0) ? doc.abstract : 'N/A' }</dd>
				<dt>Authors</dt>
				<dd>${(fn:length(doc.authors) > 0) ? doc.authors : 'N/A' }</dd>
				<dt>Corporates</dt>
				<dd>${(fn:length(doc.corporates) > 0) ? doc.corporates : 'N/A' }</dd>
				<dt>Publisher</dt>
				<dd>${(fn:length(doc.publisher) > 0) ? doc.publisher : 'N/A' }</dd>
				<dt>Publication year</dt>
				<dd>${doc.pubyear != null ? doc.pubyear : 'N/A' }</dd>
				<dt>ISBN</dt>
				<dd>${doc.isbn}</dd>
				
				<dt>&nbsp;</dt>
				
				<dt>Subject</dt>
				<dd>${(fn:length(doc.subject) > 0) ? doc.subject : 'N/A' }</dd>
				<dt>Class</dt>
				<dd>${(fn:length(doc.class) > 0) ? doc.class : 'N/A' }</dd>
				<dt>Attached documents</dt>
				<dd>${(fn:length(doc.marcurl) > 0) ? doc.marcurl : 'N/A' }</dd>
				
				<dt>&nbsp;</dt>
				
				<dt>Published</dt>
				<dd>
					<x:choose>
						<x:when select="$pxml//collection/record/datafield[@tag='260']">
							<x:out select="$pxml//collection/record/datafield[@tag='260']"/>
						</x:when>
						<x:otherwise>
							N/A
						</x:otherwise>
					</x:choose>
				</dd>
				
				<dt>Description</dt>
				<dd>
					<x:choose>
						<x:when select="$pxml//collection/record/datafield[@tag='300']">
							<x:out select="$pxml//collection/record/datafield[@tag='300']"/>
						</x:when>
						<x:otherwise>
							N/A
						</x:otherwise>
					</x:choose>
				</dd>
				
				<dt>Note</dt>
				<dd>
					<x:choose>
						<x:when select="$pxml//collection/record/datafield[@tag='500']">
							<x:out select="$pxml//collection/record/datafield[@tag='500']"/>
						</x:when>
						<x:otherwise>
							N/A
						</x:otherwise>
					</x:choose>
				</dd>
				
				<dt>&nbsp;</dt>
				
				<dt>XML</dt>
				<dd>
					<pre class="xml"><c:out escapeXml="true" value="${doc.prettyxml}"/></pre>
				</dd>
				
			</dl>
			
			<div class="clear"></div>
		</div>
		
		<c:import url="inc/footer.jsp"/>
	</div>
</body>
</html>