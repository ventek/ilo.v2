<%@ page language="java" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" session="false" %>
<%@ include file="/WEB-INF/jsp/inc/include.jsp" %>

<ul id="skip_to">
    <li><a href="#main-content"><fmt:message key="header.skiptocontent"/></a></li>
</ul>

<header id="banner" role="banner">

    <div id="primary-banner">
        <div class="container">
            <div class="row">
                <div class="large-6-col">
                    <a href="/global" id="logo" class="navbar-brand"><h1><fmt:message key="header.ilo.title"/></h1></a>
                </div>
                <div class="large-6-col">
                    <h2><fmt:message key="header.h2"/></h2>
                    <h3><fmt:message key="header.h3"/></h3>
                    <%--
                    <div class="language">
                        <a href="#" class="open">Language <span class="icon-angle-down"></span></a>
                        <ul class="languages">
                            <c:url var="url" value="${httpRequest.searchURI}">
                                <c:param name="action" value="${httpRequest.action}"/>
                                <c:if test="${httpRequest.action == 'search'}">
                                    <c:param name="searchWhat" value="${httpRequest.searchWhat}"/>
                                    <c:param name="navigators" value="${httpRequest.navigators}"/>
                                    <c:param name="sortby" value="${httpRequest.sortby}"/>
                                    <c:param name="lastDay" value="${httpRequest.lastDay}"/>
                                </c:if>
                            </c:url>

                            <c:if test="${pageLocale.currentLocale.language != 'en'}">
                                <li><a href="${url}&locale=en_US&searchLanguage=en">English</a></li>
                            </c:if>
                            <c:if test="${pageLocale.currentLocale.language != 'fr'}">
                                <li><a href="${url}&locale=fr_FR&searchLanguage=fr">Français</a></li>
                            </c:if>
                            <c:if test="${pageLocale.currentLocale.language != 'es'}">
                                <li><a href="${url}&locale=es_ES&searchLanguage=es">Español</a></li>
                            </c:if>
                        </ul>
                    </div>
                    --%>
                </div>
            </div>
        </div>
    </div><!-- End primary-banner -->


    <div id="secondary-banner">
        <div class="container">
            <div class="row">
                <div class="large-7-col">
                    <nav role="navigation">
                        <div class="">
                            <ul role="menu" class="nav navbar-nav">
                                <li class="fast-nav"><fmt:message key="header.fastnavigation"/>:</li>
                                <li class="dropdown" role="menuitem">
                                    <a href="#" class="dropdown-toggle"><fmt:message key="header.regions.title"/></a>
                                    <div class="mega-dropdown regions">
                                        <ul>
                                            <li class="africa"><a href="/addisababa/lang--en/index.htm"><span class="region-tag"><fmt:message key="header.regions.africa"/></span> <span class="icon-map-marker"></span></a></li>
                                            <li class="americas"><a href="/americas"><span class="region-tag"><fmt:message key="header.regions.americas"/></span> <span class="icon-map-marker"></span></a></li>
                                            <li class="arab-states"><a href="/public/english/region/arpro/beirut/index.htm"><span class="region-tag"><fmt:message key="header.regions.arab-states"/></span> <span class="icon-map-marker"></span></a></li>
                                            <li class="asia-and-the-pacific"><a href="/asia/lang--en/index.htm"><span class="region-tag"><fmt:message key="header.regions.asia-and-the-pacific"/></span> <span class="icon-map-marker"></span></a></li>
                                            <li class="europe-and-central-asia"><a href="/public/english/region/eurpro/geneva/index.htm"><span class="region-tag"><fmt:message key="header.regions.europe-and-central-asia"/></span> <span class="icon-map-marker"></span></a></li>
                                        </ul>
                                    </div>
                                </li>
                                <%-- var i = 1, s = ''; $('.navbar-nav').find('> li:eq(2)').find('li').each(function(k,li) { s += i + ' = ' + $(li).text().trim() + '\n'; i++; }); s; --%>
                                <li class="dropdown" role="menuitem">
                                    <a href="#" class="dropdown-toggle"><fmt:message key="header.topic.title"/></a>
                                    <div class="mega-dropdown topics">
                                        <ul>
                                        <%-- Just copy, some reformatting, and paste --%>
                                            <c:choose>
                                                <c:when test="${pageLocale.currentLocale.language == 'en'}">
                                                    <li>
                                                        <a href="/global/topics/child-labour/lang--en/index.htm">Child Labour</a>
                                                    </li>
                                                    <li>
                                                        <a href="/global/topics/employment-promotion/cooperatives/lang--en/index.htm">Cooperatives</a>
                                                    </li>
                                                    <li>
                                                        <a href="/global/topics/decent-work/lang--en/index.htm">Decent work</a>
                                                    </li>
                                                    <li>
                                                        <a href="/global/topics/skills-knowledge-and-employability/disability-and-work/lang--en/index.htm">Disability and work</a>
                                                    </li>
                                                    <li>
                                                        <a href="/global/topics/domestic-workers/lang--en/index.htm">Domestic workers</a>
                                                    </li>
                                                    <li>
                                                        <a href="/global/topics/economic-and-social-development/lang--en/index.htm">Economic and social development</a>
                                                    </li>
                                                    <li>
                                                        <a href="/global/topics/employment-promotion/lang--en/index.htm">Employment promotion</a>
                                                    </li>
                                                    <li>
                                                        <a href="/global/topics/employment-security/lang--en/index.htm">Employment security</a>
                                                    </li>
                                                    <li>
                                                        <a href="/global/topics/equality-and-discrimination/lang--en/index.htm">Equality and discrimination</a>
                                                    </li>
                                                    <li>
                                                        <a href="/global/topics/forced-labour/lang--en/index.htm">Forced labour, human trafficking and slavery</a>
                                                    </li>
                                                    <li>
                                                        <a href="/global/topics/freedom-of-association-and-the-right-to-collective-bargaining/lang--en/index.htm">Freedom of association</a>
                                                    </li>
                                                    <li>
                                                        <a href="/global/topics/equality-and-discrimination/gender-equality/lang--en/index.htm">Gender equality</a>
                                                    </li>
                                                    <li>
                                                        <a href="/global/topics/economic-and-social-development/globalization/lang--en/index.htm">Globalization</a>
                                                    </li>
                                                    <li>
                                                        <a href="/global/topics/green-jobs/lang--en/index.htm">Green jobs</a>
                                                    </li>
                                                    <li>
                                                        <a href="/global/topics/hiv-aids/lang--en/index.htm">HIV/AIDS</a>
                                                    </li>
                                                    <li>
                                                        <a href="/global/topics/equality-and-discrimination/indigenous-and-tribal-peoples/lang--en/index.htm">Indigenous and tribal peoples</a>
                                                    </li>
                                                    <li>
                                                        <a href="http://www.ilo.org/public/english/dialogue/sector/index.htm">Industries and sectors</a>
                                                    </li>
                                                    <li>
                                                        <a href="/global/topics/employment-promotion/informal-economy/lang--en/index.htm">Informal sector</a>
                                                    </li>
                                                    <li>
                                                        <a href="/global/topics/labour-administration-inspection/lang--en/index.htm">Labour inspection and administration</a>
                                                    </li>
                                                    <li>
                                                        <a href="/global/topics/labour-law/lang--en/index.htm">Labour law</a>
                                                    </li>
                                                    <li>
                                                        <a href="/global/topics/labour-migration/lang--en/index.htm">Labour migration</a>
                                                    </li>
                                                    <li>
                                                        <a href="/global/standards/maritime-labour-convention/lang--en/index.htm">Maritime Labour Convention</a>
                                                    </li>
                                                    <li>
                                                        <a href="/global/topics/equality-and-discrimination/maternity-protection/lang--en/index.htm">Maternity protection</a>
                                                    </li>
                                                    <li>
                                                        <a href="/global/topics/employment-promotion/microfinance/lang--en/index.htm">Microfinance</a>
                                                    </li>
                                                    <li>
                                                        <a href="/global/topics/millennium-development-goals/lang--en/index.htm">Millennium Development Goals</a>
                                                    </li>
                                                    <li>
                                                        <a href="/global/topics/employment-promotion/multinational-enterprises/lang--en/index.htm">Multinational enterprises</a>
                                                    </li>
                                                    <li>
                                                        <a href="/global/topics/post-2015/lang--en/index.htm">Post-2015 Development Agenda</a>
                                                    </li>
                                                    <li>
                                                        <a href="/global/topics/employment-promotion/recovery-and-reconstruction/lang--en/index.htm">Recovery and reconstruction</a>
                                                    </li>
                                                    <li>
                                                        <a href="/global/topics/economic-and-social-development/rural-development/lang--en/index.htm">Rural development</a>
                                                    </li>
                                                    <li>
                                                        <a href="/global/topics/safety-and-health-at-work/lang--en/index.htm">Safety and health at work</a>
                                                    </li>
                                                    <li>
                                                        <a href="/global/topics/skills-knowledge-and-employability/lang--en/index.htm">Skills, Knowledge and Employability</a>
                                                    </li>
                                                    <li>
                                                        <a href="/global/topics/employment-promotion/small-enterprises/lang--en/index.htm">Small enterprises</a>
                                                    </li>
                                                    <li>
                                                        <a href="/global/topics/social-security/lang--en/index.htm">Social security</a>
                                                    </li>
                                                    <li>
                                                        <a href="/global/topics/employment-promotion/sustainable-enterprises/lang--en/index.htm">Sustainable enterprises</a>
                                                    </li>
                                                    <li>
                                                        <a href="/global/topics/workers-and-employers-organizations-tripartism-and-social-dialogue/lang--en/index.htm">Tripartism and social dialogue</a>
                                                    </li>
                                                    <li>
                                                        <a href="/global/topics/working-conditions/wages/lang--en/index.htm">Wages</a>
                                                    </li>
                                                    <li>
                                                        <a href="/global/topics/working-conditions/lang--en/index.htm">Working conditions</a>
                                                    </li>
                                                    <li>
                                                        <a href="/global/topics/working-conditions/working-time/lang--en/index.htm">Working time</a>
                                                    </li>
                                                    <li>
                                                        <a href="/global/topics/youth-employment/lang--en/index.htm">Youth employment</a>
                                                    </li>
                                                </c:when>
                                                <c:when test="${pageLocale.currentLocale.language == 'fr'}">
                                                    <li>
                                                        <a href="/global/topics/labour-law/labour-administration/lang--fr/index.htm">Administration du travail </a>
                                                    </li>
                                                    <li>
                                                        <a href="/global/topics/employment-promotion/cooperatives/lang--fr/index.htm">Coopératives</a>
                                                    </li>
                                                    <li>
                                                        <a href="/global/topics/skills-knowledge-and-employability/lang--fr/index.htm">Compétences, savoirs et employabilité</a>
                                                    </li>
                                                    <li>
                                                        <a href="/global/topics/working-conditions/lang--fr/index.htm">Conditions de travail</a>
                                                    </li>
                                                    <li>
                                                        <a href="/global/topics/economic-and-social-development/lang--fr/index.htm">Développement économique et social</a>
                                                    </li>
                                                    <li>
                                                        <a href="/global/topics/labour-law/lang--fr/index.htm">Droit du travail</a>
                                                    </li>
                                                    <li>
                                                        <a href="/global/topics/equality-and-discrimination/lang--fr/index.htm">Egalité et discrimination</a>
                                                    </li>
                                                    <li>
                                                        <a href="/global/topics/youth-employment/lang--fr/index.htm">Emploi des jeunes</a>
                                                    </li>
                                                    <li>
                                                        <a href="/global/topics/employment-promotion/sustainable-enterprises/lang--fr/index.htm">Entreprises durables</a>
                                                    </li>
                                                    <li>
                                                        <a href="/global/topics/employment-promotion/microfinance/lang--fr/index.htm">Entreprises multinationales</a>
                                                    </li>
                                                    <li>
                                                        <a href="/global/topics/employment-promotion/corporate-citizenship/lang--fr/index.htm">Economie informelle</a>
                                                    </li>
                                                    <li>
                                                        <a href="/global/topics/equality-and-discrimination/gender-equality/lang--fr/index.htm">Egalité des genres</a>
                                                    </li>
                                                    <li>
                                                        <a href="/global/topics/skills-knowledge-and-employability/disability-and-work/lang--fr/index.htm">Handicap et travail </a>
                                                    </li>
                                                    <li>
                                                        <a href="/global/topics/freedom-of-association-and-the-right-to-collective-bargaining/lang--fr/index.htm">Libert&eacute; d'association et le droit de n&eacute;gociation collective</a>
                                                    </li>
                                                    <li>
                                                        <a href="/global/topics/labour-migration/lang--fr/index.htm">Migration</a>
                                                    </li>
                                                    <li>
                                                        <a href="/global/topics/millennium-development-goals/lang--fr/index.htm">Millennium Development Goals</a>
                                                    </li>
                                                    <li>
                                                        <a href="/global/topics/economic-and-social-development/globalization/lang--fr/index.htm">Mondialisation </a>
                                                    </li>
                                                    <li>
                                                        <a href="/global/topics/employment-promotion/local-economic-development/lang--fr/index.htm">Microfinance</a>
                                                    </li>
                                                    <li>
                                                        <a href="/global/topics/employment-promotion/lang--fr/index.htm">Promotion de l'emploi</a>
                                                    </li>
                                                    <li>
                                                        <a href="/global/topics/employment-promotion/recovery-and-reconstruction/lang--fr/index.htm">Petites entreprises</a>
                                                    </li>
                                                    <li>
                                                        <a href="/global/topics/equality-and-discrimination/maternity-protection/lang--fr/index.htm">Protection de la maternité</a>
                                                    </li>
                                                    <li>
                                                        <a href="/global/topics/equality-and-discrimination/indigenous-and-tribal-peoples/lang--fr/index.htm">Peuples indigènes et tribaux</a>
                                                    </li>
                                                    <li>
                                                        <a href="/global/topics/post-2015/lang--fr/index.htm">Programme de d&eacute;veloppement post 2015</a>
                                                    </li>
                                                    <li>
                                                        <a href="/global/topics/employment-promotion/multinational-enterprises/lang--fr/index.htm">Redressement et reconstruction</a>
                                                    </li>
                                                    <li>
                                                        <a href="/global/topics/working-conditions/wages/lang--fr/index.htm">Salaires </a>
                                                    </li>
                                                    <li>
                                                        <a href="/global/topics/social-security/lang--fr/index.htm">Sécurité sociale</a>
                                                    </li>
                                                    <li>
                                                        <a href="/global/topics/safety-and-health-at-work/lang--fr/index.htm">Sécurité et santé au travail</a>
                                                    </li>
                                                    <li>
                                                        <a href="/global/topics/employment-security/lang--fr/index.htm">Sécurité de l'emploi</a>
                                                    </li>
                                                    <li>
                                                        <a href="http://www.ilo.org/public/french/dialogue/sector/index.htm">Secteurs et industries</a>
                                                    </li>
                                                    <li>
                                                        <a href="/global/topics/working-conditions/working-time/lang--fr/index.htm">Temps de travail</a>
                                                    </li>
                                                    <li>
                                                        <a href="/global/topics/domestic-workers/lang--fr/index.htm">Travail domestique</a>
                                                    </li>
                                                    <li>
                                                        <a href="/global/topics/child-labour/lang--fr/index.htm">Travail des enfants</a>
                                                    </li>
                                                    <li>
                                                        <a href="/global/topics/decent-work/lang--fr/index.htm">Travail d&eacute;cent</a>
                                                    </li>
                                                    <li>
                                                        <a href="/global/topics/forced-labour/lang--fr/index.htm">Travail forcé, traite des êtres humains et esclavage</a>
                                                    </li>
                                                    <li>
                                                        <a href="/global/topics/workers-and-employers-organizations-tripartism-and-social-dialogue/lang--fr/index.htm">Tripartisme et dialogue social</a>
                                                    </li>
                                                    <li>
                                                        <a href="/global/topics/hiv-aids/lang--fr/index.htm">VIH/SIDA</a>
                                                    </li>
                                                </c:when>
                                                <c:when test="${pageLocale.currentLocale.language == 'es'}">
                                                    <li>
                                                        <a href="/global/topics/labour-administration-inspection/lang--es/index.htm">Administración y inspección del trabajo</a>
                                                    </li>
                                                    <li>
                                                        <a href="/global/topics/post-2015/lang--es/index.htm">Agenda de desarrollo post 2015</a>
                                                    </li>
                                                    <li>
                                                        <a href="/global/topics/working-conditions/lang--es/index.htm">Condiciones de trabajo</a>
                                                    </li>
                                                    <li>
                                                        <a href="/global/topics/skills-knowledge-and-employability/lang--es/index.htm">Conocimientos teóricos y prácticos y empleabilidad</a>
                                                    </li>
                                                    <li>
                                                        <a href="/global/topics/employment-promotion/cooperatives/lang--es/index.htm">Cooperativas</a>
                                                    </li>
                                                    <li>
                                                        <a href="/global/topics/labour-law/lang--es/index.htm">Derecho del trabajo</a>
                                                    </li>
                                                    <li>
                                                        <a href="/global/topics/economic-and-social-development/lang--es/index.htm">Desarrollo económico y social</a>
                                                    </li>
                                                    <li>
                                                        <a href="/global/topics/skills-knowledge-and-employability/disability-and-work/lang--es/index.htm">Discapacidad y trabajo</a>
                                                    </li>
                                                    <li>
                                                        <a href="/global/topics/employment-promotion/informal-economy/lang--es/index.htm">Economía informal</a>
                                                    </li>
                                                    <li>
                                                        <a href="/global/topics/youth-employment/lang--es/index.htm">Empleo juvenil</a>
                                                    </li>
                                                    <li>
                                                        <a href="/global/topics/green-jobs/lang--es/index.htm">Empleos verdes</a>
                                                    </li>
                                                    <li>
                                                        <a href="/global/topics/employment-promotion/multinational-enterprises/lang--es/index.htm">Empresas multinacionales</a>
                                                    </li>
                                                    <li>
                                                        <a href="/global/topics/employment-promotion/sustainable-enterprises/lang--es/index.htm">Empresas sostenibles</a>
                                                    </li>
                                                    <li>
                                                        <a href="/global/topics/economic-and-social-development/globalization/lang--es/index.htm">Globalización </a>
                                                    </li>
                                                    <li>
                                                        <a href="/global/topics/equality-and-discrimination/gender-equality/lang--es/index.htm">Igualdad de género</a>
                                                    </li>
                                                    <li>
                                                        <a href="/global/topics/equality-and-discrimination/lang--es/index.htm">Igualdad y discriminación</a>
                                                    </li>
                                                    <li>
                                                        <a href="/global/topics/freedom-of-association-and-the-right-to-collective-bargaining/lang--es/index.htm">Libertad sindical y derecho a la negociación colectiva</a>
                                                    </li>
                                                    <li>
                                                        <a href="/global/standards/maritime-labour-convention/lang--es/index.htm">Maritime Labour Convention</a>
                                                    </li>
                                                    <li>
                                                        <a href="/global/topics/employment-promotion/microfinance/lang--es/index.htm">Microfinanciamento</a>
                                                    </li>
                                                    <li>
                                                        <a href="/global/topics/labour-migration/lang--es/index.htm">Migración laboral</a>
                                                    </li>
                                                    <li>
                                                        <a href="/global/topics/millennium-development-goals/lang--es/index.htm">Millennium Development Goals</a>
                                                    </li>
                                                    <li>
                                                        <a href="/global/topics/employment-promotion/small-enterprises/lang--es/index.htm">Pequeñas empresas</a>
                                                    </li>
                                                    <li>
                                                        <a href="/global/topics/employment-promotion/lang--es/index.htm">Promoción del empleo</a>
                                                    </li>
                                                    <li>
                                                        <a href="/global/topics/equality-and-discrimination/maternity-protection/lang--es/index.htm">Protección de la maternidad</a>
                                                    </li>
                                                    <li>
                                                        <a href="/global/topics/equality-and-discrimination/indigenous-and-tribal-peoples/lang--es/index.htm">Pueblos indígenas y tribales</a>
                                                    </li>
                                                    <li>
                                                        <a href="/global/topics/employment-promotion/recovery-and-reconstruction/lang--es/index.htm">Recuperación y reconstrucción</a>
                                                    </li>
                                                    <li>
                                                        <a href="/global/topics/working-conditions/wages/lang--es/index.htm">Salarios</a>
                                                    </li>
                                                    <li>
                                                        <a href="/global/topics/safety-and-health-at-work/lang--es/index.htm">Salud y seguridad en trabajo</a>
                                                    </li>
                                                    <li>
                                                        <a href="http://www.ilo.org/public/english/dialogue/sector/index.htm">Sectores y industrias</a>
                                                    </li>
                                                    <li>
                                                        <a href="/global/topics/employment-security/lang--es/index.htm">Seguridad del empleo</a>
                                                    </li>
                                                    <li>
                                                        <a href="/global/topics/social-security/lang--es/index.htm">Seguridad social</a>
                                                    </li>
                                                    <li>
                                                        <a href="/global/topics/working-conditions/working-time/lang--es/index.htm">Tiempo de trabajo</a>
                                                    </li>
                                                    <li>
                                                        <a href="/global/topics/domestic-workers/lang--es/index.htm">Trabajadores domésticos</a>
                                                    </li>
                                                    <li>
                                                        <a href="/global/topics/child-labour/lang--es/index.htm">Trabajo infantil</a>
                                                    </li>
                                                    <li>
                                                        <a href="/global/topics/decent-work/lang--es/index.htm">Trabajo decente</a>
                                                    </li>
                                                    <li>
                                                        <a href="/global/topics/forced-labour/lang--es/index.htm">Trabajo forzoso, trafico humano y esclavitud </a>
                                                    </li>
                                                    <li>
                                                        <a href="/global/topics/workers-and-employers-organizations-tripartism-and-social-dialogue/lang--es/index.htm">Tripartismo y diálogo social</a>
                                                    </li>
                                                    <li>
                                                        <a href="/global/topics/hiv-aids/lang--es/index.htm">VIH/SIDA</a>
                                                    </li>
                                                </c:when>
                                            </c:choose>
                                        </ul>
                                    </div>
                                </li>
                                <li class="dropdown" role="menuitem">
                                    <a href="#" class="dropdown-toggle"><fmt:message key="header.sectors.title"/></a>
                                    <div class="mega-dropdown sectors">
                                        <ul>
                                            <c:choose>
                                                <c:when test="${pageLocale.currentLocale.language == 'en'}">
                                                    <li>
                                                        <a href="/global/industries-and-sectors/agriculture-plantations-other-rural-sectors/lang--en/index.htm">Agriculture; plantations;other rural sectors</a>
                                                    </li>
                                                    <li>
                                                        <a href="/global/industries-and-sectors/basic-metal-production/lang--en/index.htm">Basic Metal Production</a>
                                                    </li>
                                                    <li>
                                                        <a href="/global/industries-and-sectors/chemical-industries/lang--en/index.htm">Chemical industries</a><br /><br />
                                                    </li>
                                                    <li>
                                                        <a href="/global/industries-and-sectors/construction/lang--en/index.htm">Construction</a>
                                                    </li>
                                                    <li>
                                                        <a href="/global/industries-and-sectors/education/lang--en/index.htm">Education</a>
                                                    </li>
                                                    <li>
                                                        <a href="/global/industries-and-sectors/food-drink-tobacco/lang--en/index.htm">Food; drink; tobacco</a>
                                                    </li>
                                                    <li>
                                                        <a href="/global/industries-and-sectors/forestry-wood-pulp-and-paper/lang--en/index.htm">Forestry; wood; pulp and paper</a>
                                                    </li>
                                                    <li>
                                                        <a href="/global/industries-and-sectors/health-services/lang--en/index.htm">Health services</a>
                                                    </li>
                                                    <li>
                                                        <a href="/global/industries-and-sectors/hotels-catering-tourism/lang--en/index.htm">Hotels; tourism; catering</a>
                                                    </li>
                                                    <li>
                                                        <a href="/global/industries-and-sectors/mining/lang--en/index.htm">Mining (coal; other mining)</a>
                                                    </li>
                                                    <li>
                                                        <a href="/global/industries-and-sectors/mechanical-and-electrical-engineering/lang--en/index.htm">Mechanical and electrical engineering</a>
                                                    </li>
                                                    <li>
                                                        <a href="/global/industries-and-sectors/media-culture-graphical/lang--en/index.htm">Media; culture; graphical</a>
                                                    </li>
                                                    <li>
                                                        <a href="/global/industries-and-sectors/oil-and-gas-production-oil-refining/lang--en/index.htm">Oil and gas production; oil refining</a>
                                                    </li>
                                                    <li>
                                                        <a href="/global/industries-and-sectors/postal-and-telecommunications-services/lang--en/index.htm">Postal and telecommunications services</a>
                                                    </li>
                                                    <li>
                                                        <a href="/global/industries-and-sectors/public-service/lang--en/index.htm">Public service</a>
                                                    </li>
                                                    <li>
                                                        <a href="/global/industries-and-sectors/shipping-ports-fisheries-inland-waterways/lang--en/index.htm">Shipping; ports; fisheries; inland waterways</a>
                                                    </li>
                                                    <li>
                                                        <a href="/global/industries-and-sectors/textiles-clothing-leather-footwear/lang--en/index.htm">Textiles; clothing; leather; footwear</a>
                                                    </li>
                                                    <li>
                                                        <a href="/global/industries-and-sectors/transport-including-civil-aviation-railways-road-transport/lang--en/index.htm">Transport (including civil aviation; railways; road transport)</a>
                                                    </li>
                                                    <li>
                                                        <a href="/global/industries-and-sectors/transport-equipment-manufacturing/lang--en/index.htm">Transport equipment manufacturing</a>
                                                    </li>
                                                    <li>
                                                        <a href="/global/industries-and-sectors/utilities-water-gas-electricity/lang--en/index.htm">Utilities (water; gas; electricity)</a>
                                                    </li>
                                                </c:when>
                                                <c:when test="${pageLocale.currentLocale.language == 'fr'}">
                                                    <li>
                                                        <a href="http://www.ilo.org/public/french/dialogue/sector/sectors/agri.htm">Agriculture; plantations;<br /> autres secteurs ruraux<br /></a>
                                                    </li>
                                                    <li>
                                                        <a href="http://www.ilo.org/public/french/dialogue/sector/sectors/commerce.htm">Commerce<br /></a>
                                                    </li>
                                                    <li>
                                                        <a href="http://www.ilo.org/public/french/dialogue/sector/sectors/constr.htm">Construction</a>
                                                    </li>
                                                    <li>
                                                        <a href="http://www.ilo.org/public/french/dialogue/sector/sectors/educat.htm">Education</a>
                                                    </li>
                                                    <li>
                                                        <a href="http://www.ilo.org/public/french/dialogue/sector/sectors/mee.htm">Entreprises m&eacute;caniques<br />et &eacute;lectroniques</a>
                                                    </li>
                                                    <li>
                                                        <a href="http://www.ilo.org/public/french/dialogue/sector/sectors/pubserv.htm">Fonction publique</a>
                                                    </li>
                                                    <li>
                                                        <a href="http://www.ilo.org/public/french/dialogue/sector/sectors/forest.htm">Foresterie; bois;<br />p&acirc;te et papier</a>
                                                    </li>
                                                    <li>
                                                        <a href="http://www.ilo.org/public/french/dialogue/sector/sectors/tourism.htm">H&ocirc;tels; tourisme; restauration</a>
                                                    </li>
                                                    <li>
                                                        <a href="http://www.ilo.org/public/french/dialogue/sector/sectors/chem.htm">Industries chimiques</a>
                                                    </li>
                                                    <li>
                                                        <a href="http://www.ilo.org/public/french/dialogue/sector/sectors/te.htm">Industrie du mat&eacute;riel<br />de transport</a>
                                                    </li>
                                                    <li>
                                                        <a href="http://www.ilo.org/public/french/dialogue/sector/sectors/mining.htm">Industrie mini&egrave;re;<br />(charbon;autres min&eacute;raux)</a>
                                                    </li>
                                                    <li>
                                                        <a href="http://www.ilo.org/public/french/dialogue/sector/sectors/media.htm">M&eacute;dias; culture; images</a>
                                                    </li>
                                                    <li>
                                                        <a href="http://www.ilo.org/public/french/dialogue/sector/sectors/metal.htm">Production de m&eacute;tal de base</a>
                                                    </li>
                                                    <li>
                                                        <a href="http://www.ilo.org/public/french/dialogue/sector/sectors/oilgas.htm">Production de p&eacute;trole et<br />de gaz; raffinerie de p&eacute;trole</a>
                                                    </li>
                                                    <li>
                                                        <a href="http://www.ilo.org/public/french/dialogue/sector/sectors/food.htm">Produits alimentaires;<br />boissons; tabac</a>
                                                    </li>
                                                    <li>
                                                        <a href="http://www.ilo.org/public/french/dialogue/sector/sectors/health.htm">Services de sant&eacute;</a>
                                                    </li>
                                                    <li>
                                                        <a href="http://www.ilo.org/public/french/dialogue/sector/sectors/service.htm">Services financiers;<br />Services professionnels</a>
                                                    </li>
                                                    <li>
                                                        <a href="http://www.ilo.org/public/french/dialogue/sector/sectors/postal.htm">Services postaux et<br />services de t&eacute;l&eacute;communications</a>
                                                    </li>
                                                    <li>
                                                        <a href="http://www.ilo.org/public/french/dialogue/sector/sectors/utilities.htm">Services publics de distribution (eau; gaz; &eacute;lectricit&eacute;)</a>
                                                    </li>
                                                    <li>
                                                        <a href="http://www.ilo.org/public/french/dialogue/sector/sectors/textile.htm">Textiles; v&ecirc;tement;<br />cuir; chaussure</a>
                                                    </li>
                                                    <li>
                                                        <a href="http://www.ilo.org/public/french/dialogue/sector/sectors/mariti.htm">Transport maritime; activit&eacute;s portuaires;p&ecirc;che;transports<br />int&eacute;rieurs par voies d'eau</a>
                                                    </li>
                                                    <li>
                                                        <a href="http://www.ilo.org/public/french/dialogue/sector/sectors/transp.htm">Transport (&eacute;galement<br />l'aviation civile;trains;<br />transport routier)</a>
                                                    </li>
                                                </c:when>
                                                <c:when test="${pageLocale.currentLocale.language == 'es'}">
                                                    <li>
                                                        <a href="http://www.ilo.org/public/spanish/dialogue/sector/sectors/agri.htm">Agricultura; plantaciones,<br />otros sectores rurales</a>
                                                    </li>
                                                    <li>
                                                        <a href="http://www.ilo.org/public/spanish/dialogue/sector/sectors/food.htm">Alimentaci&oacute;n; bebidas; tabaco </a>
                                                    </li>
                                                    <li>
                                                        <a href="http://www.ilo.org/public/spanish/dialogue/sector/sectors/commerce.htm">Comercio</a>
                                                    </li>
                                                    <li>
                                                        <a href="http://www.ilo.org/public/spanish/dialogue/sector/sectors/constr.htm">Construcci&oacute;n</a>
                                                    </li>
                                                    <li>
                                                        <a href="http://www.ilo.org/public/spanish/dialogue/sector/sectors/educat.htm">Educaci&oacute;n</a>
                                                    </li>
                                                    <li>
                                                        <a href="http://www.ilo.org/public/spanish/dialogue/sector/sectors/te.htm">Fabricaci&oacute;n de material de transporte</a>
                                                    </li>
                                                    <li>
                                                        <a href="http://www.ilo.org/public/spanish/dialogue/sector/sectors/pubserv.htm">Funci&oacute;n p&uacute;blica</a>
                                                    </li>
                                                    <li>
                                                        <a href="http://www.ilo.org/public/spanish/dialogue/sector/sectors/tourism.htm">Hoteler&iacute;a, restauraci&oacute;n, turismo</a>
                                                    </li>
                                                    <li>
                                                        <a href="http://www.ilo.org/public/spanish/dialogue/sector/sectors/chem.htm">Industrias qu&iacute;micas</a>
                                                    </li>
                                                    <li>
                                                        <a href="http://www.ilo.org/public/spanish/dialogue/sector/sectors/mee.htm">Ingenieria mec&aacute;nica<br />y el&eacute;ctria</a>
                                                    </li>
                                                    <li>
                                                        <a href="http://www.ilo.org/public/spanish/dialogue/sector/sectors/media.htm">Medios de comunicaci&oacute;n;<br />cultura; gr&aacute;ficos</a>
                                                    </li>
                                                    <li>
                                                        <a href="http://www.ilo.org/public/spanish/dialogue/sector/sectors/mining.htm">Miner&iacute;a (carb&oacute;n, otra miner&iacute;a)</a>
                                                    </li>
                                                    <li>
                                                        <a href="http://www.ilo.org/public/spanish/dialogue/sector/sectors/oilgas.htm">Petroleo y producci&oacute;n de gas;<br />refinaci&oacute;n de petroleo</a>
                                                    </li>
                                                    <li>
                                                        <a href="http://www.ilo.org/public/spanish/dialogue/sector/sectors/metal.htm">Producci&oacute;n de metales b&aacute;sicos</a>
                                                    </li>
                                                    <li>
                                                        <a href="http://www.ilo.org/public/spanish/dialogue/sector/sectors/postal.htm">Servicios de correos<br />y de telecomunicaciones</a>
                                                    </li>
                                                    <li>
                                                        <a href="http://www.ilo.org/public/spanish/dialogue/sector/sectors/health.htm">Servicios de salud</a>
                                                    </li>
                                                    <li>
                                                        <a href="http://www.ilo.org/public/spanish/dialogue/sector/sectors/service.htm">Servicios financieros;<br />servicios profesionales</a>
                                                    </li>
                                                    <li>
                                                        <a href="http://www.ilo.org/public/spanish/dialogue/sector/sectors/utilities.htm">Servicios p&uacute;blicos (agua;<br />gas; electricidad)</a>
                                                    </li>
                                                    <li>
                                                        <a href="http://www.ilo.org/public/spanish/dialogue/sector/sectors/forest.htm">Silvicultura; madera;<br />celulosa; papel</a>
                                                    </li>
                                                    <li>
                                                        <a href="http://www.ilo.org/public/spanish/dialogue/sector/sectors/textile.htm">Textiles; vestido; cuero; calzado</a>
                                                    </li>
                                                    <li>
                                                        <a href="http://www.ilo.org/public/spanish/dialogue/sector/sectors/transp.htm">Transporte (inluyendo aviaci&oacute;n civil; ferrocarriles; transporte<br />por carretera)</a>
                                                    </li>
                                                    <li>
                                                        <a href="http://www.ilo.org/public/spanish/dialogue/sector/sectors/mariti.htm">Transporte mar&iacute;timo; puertos;<br />pesca; transporte interior</a>
                                                    </li>
                                                </c:when>
                                            </c:choose>
                                        </ul>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </div>
                <%--
                <div class="large-5-col small-12-col">
                    <form class="form-search pull-right" role="search">
                        <label for="keywords">Search:</label>
                        <input type="text" name="keywords" id="keywords" class="input-medium search-query" placeholder="Search the site">
                        <button type="submit" class="btn menu-trigger">Search</button>
                    </form>
                </div>
                --%>
            </div>
        </div>
    </div><!-- End secondary-banner -->


    <div id="tertiary-banner">

        <div class="container">
            <div class="row">
                <nav class="large-12-col" role="navigation">

                    <a class="navbar-toggle" href="#">
                        <div class="pull-left">
                            <span class="menu"><fmt:message key="header.menu"/></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </div>
                    </a>

                    <div class="collapse">
                        <ul role="menu" class="nav navbar-nav" id="menu">
                            <li role="menuitem" class=""><a href="/global/lang--${pageLocale.currentLocale.language}/index.htm"><span class="home-link"><fmt:message key="header.menu.home"/></span></a></li>
                            <li role="menuitem" class=""><a href="/global/about-the-ilo/lang--${pageLocale.currentLocale.language}/index.htm"><fmt:message key="header.menu.about-the-ilo"/></a></li>
                            <li role="menuitem" class=""><a href="/global/newsroom/lang--${pageLocale.currentLocale.language}/index.htm"><fmt:message key="header.menu.newsroom"/></a></li>
                            <li role="menuitem" class=""><a href="/global/meetings-and-events/lang--${pageLocale.currentLocale.language}/index.htm"><fmt:message key="header.menu.meetings-and-events"/></a></li>
                            <li role="menuitem" class=""><a href="/global/publications/lang--${pageLocale.currentLocale.language}/index.htm"><fmt:message key="header.menu.publications"/></a></li>
                            <li role="menuitem" class=""><a href="/global/research/lang--${pageLocale.currentLocale.language}/index.htm"><fmt:message key="header.menu.research"/></a></li>
                            <li role="menuitem" class=""><a href="/global/standards/lang--${pageLocale.currentLocale.language}/index.htm"><fmt:message key="header.menu.standards"/></a></li>
                            <li role="menuitem" class=""><a href="/global/statistics-and-databases/lang--${pageLocale.currentLocale.language}/index.htm"><fmt:message key="header.menu.statistics-and-databases"/></a></li>
                            <li role="menuitem" class=""><a href="/global/contact-us/lang--${pageLocale.currentLocale.language}/index.htm"><fmt:message key="header.menu.contact-us"/></a></li>
                            <li role="menuitem" class="here"><a href="#"><fmt:message key="search.label.search"/></a></li>
                        </ul>
                    </div>

                </nav>
            </div>
        </div>

    </div><!-- End tertiary-banner -->

    <%--
    <div class="social-pages">
        <ul>
            <li class="social-link"><a href="#"><span>Follow us on Twitter</span> <img src="assets/images/templates/icon-twitter.png" alt="Follow us on Twitter"></a></li>
            <li class="social-link"><a href="#"><span>Find us on YouTube</span> <img src="assets/images/templates/icon-youtube.png" alt="Find us on YouTube"></a></li>
            <li class="social-link"><a href="#"><span>Find us on Facebook</span> <img src="assets/images/templates/icon-facebook.png" alt="Find us on Facebook"></a></li>
            <li class="social-link"><a href="#"><span>Find us on Flickr</span> <img src="assets/images/templates/icon-flickr.png" alt="Find us on Flickr"></a></li>
            <li class="social-link"><a href="#"><span>Find us on Linked In</span> <img src="assets/images/templates/icon-linkedin.png" alt="Find us on Linked In"></a></li>
            <li class="social-link"><a href="#"><span>RSS</span> <img src="assets/images/templates/icon-rss.png" alt="RSS"></a></li>
        </ul>
    </div>
    --%>

</header>