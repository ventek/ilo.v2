<%@ page language="java" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" session="false" %>
<%@ include file="/WEB-INF/jsp/inc/include.jsp" %>

<div class="footer">
    <footer role="contentinfo" class="container">

        <div class="row">
            <div class="large-3-col small-12-col">

                <div class="vcard">
                    <h6 class="org"><a href="#"><fmt:message key="footer.ilo.title"/></a></h6>
                    <p class="adr">
                        <span class="street-address">4 route des Morillons</span><br/>
                        <span class="postal-code">CH-1211 Genève 22</span><br/>
                        <span class="country-name"><fmt:message key="footer.ilo.country"/></span><br/>

                        <br />
                        <a target="" href="/global/contact-us/lang--${pageLocale.currentLocale.language}/index.htm"><fmt:message key="footer.ilo.contact"/></a>
                    </p>
                </div>

                <c:choose>
                    <c:when test="${pageLocale.currentLocale.language == 'fr'}">
                        <a href="#" class="logo"><img src="assets/images/templates/ilo-blue-small-fr.png" alt="<fmt:message key="footer.ilo.title"/>"><span><fmt:message key="footer.ilo.title"/></span></a>
                    </c:when>
                    <c:when test="${pageLocale.currentLocale.language == 'es'}">
                        <a href="#" class="logo"><img src="assets/images/templates/ilo-blue-small-es.png" alt="<fmt:message key="footer.ilo.title"/>"><span><fmt:message key="footer.ilo.title"/></span></a>
                    </c:when>
                    <c:otherwise>
                        <a href="#" class="logo"><img src="assets/images/templates/ilo-blue-small.png" alt="<fmt:message key="footer.ilo.title"/>"><span><fmt:message key="footer.ilo.title"/></span></a>
                    </c:otherwise>
                </c:choose>
            </div>
            <div class="large-9-col">

                <nav role="navigation" class="row">
                    <ul class="large-4-col">
                        <li><a href="/global/about-the-ilo/lang--${pageLocale.currentLocale.language}/index.htm"><fmt:message key="footer.aboutilo.0"/></a>
                            <ul>
                                <li><a href="/global/about-the-ilo/employment-opportunities/lang--${pageLocale.currentLocale.language}/index.htm"><fmt:message key="footer.aboutilo.1"/></a></li>
                                <li><a href="/global/about-the-ilo/how-the-ilo-works/departments-and-offices/lang--${pageLocale.currentLocale.language}/index.htm"><fmt:message key="footer.aboutilo.2"/></a></li>
                                <li><a href="/global/about-the-ilo/mission-and-objectives/lang--${pageLocale.currentLocale.language}/index.htm"><fmt:message key="footer.aboutilo.3"/></a></li>
                                <li><a href="/global/about-the-ilo/decent-work-agenda/lang--${pageLocale.currentLocale.language}/index.htm"><fmt:message key="footer.aboutilo.4"/></a></li>
                                <li><a href="/global/about-the-ilo/history/lang--${pageLocale.currentLocale.language}/index.htm"><fmt:message key="footer.aboutilo.5"/></a></li>
                            </ul>
                        </li>
                        <li><fmt:message key="footer.meetings.0"/>
                            <ul>
                                <li><a href="/global/meetings-and-events/international-labour-conference/lang--${pageLocale.currentLocale.language}/index.htm"><fmt:message key="footer.meetings.1"/></a></li>
                                <li><a href="/global/meetings-and-events/governing-body/lang--${pageLocale.currentLocale.language}/index.htm"><fmt:message key="footer.meetings.2"/></a></li>
                                <li><a href="/global/meetings-and-events/regional-meetings/lang--${pageLocale.currentLocale.language}/index.htm"><fmt:message key="footer.meetings.3"/></a></li>
                                <li><a href="/global/meetings-and-events/campaigns/voices-on-social-justice/lang--${pageLocale.currentLocale.language}/index.htm"><fmt:message key="footer.meetings.4"/></a></li>
                            </ul>
                        </li>
                        <li><fmt:message key="footer.tripartism.0"/>
                            <ul>
                                <li><a href="/global/about-the-ilo/how-the-ilo-works/lang--${pageLocale.currentLocale.language}/index.htm"><fmt:message key="footer.tripartism.1"/></a></li>
                                <li><a href="/global/about-the-ilo/how-the-ilo-works/lang--${pageLocale.currentLocale.language}/index.htm"><fmt:message key="footer.tripartism.2"/></a></li>
                            </ul>
                        </li>
                    </ul>
                    <ul class="large-4-col">
                        <li><a href="/global/standards/lang--${pageLocale.currentLocale.language}/index.htm"><fmt:message key="footer.labour.0"/></a>
                            <ul>
                                <li><a href="/global/standards/maritime-labour-convention/lang--${pageLocale.currentLocale.language}/index.htm"><fmt:message key="footer.labour.1"/></a></li>
                                <li><a href="#"><fmt:message key="footer.labour.2"/></a></li>
                                <li><a href="#"><fmt:message key="footer.labour.3"/></a></li>
                            </ul>
                        </li>
                        <li><a href="/global/topics/lang--${pageLocale.currentLocale.language}/index.htm"><fmt:message key="footer.topic.0"/></a>
                            <ul>
                                <li><a href="/global/topics/child-labour/lang--${pageLocale.currentLocale.language}/index.htm"><fmt:message key="footer.topic.1"/></a></li>
                                <li><a href="/global/topics/safety-and-health-at-work/lang--${pageLocale.currentLocale.language}/index.htm"><fmt:message key="footer.topic.2"/></a></li>
                                <li><a href="/global/topics/forced-labour/lang--${pageLocale.currentLocale.language}/index.htm"><fmt:message key="footer.topic.3"/></a></li>
                                <li><a href="/global/topics/green-jobs/lang--${pageLocale.currentLocale.language}/index.htm"><fmt:message key="footer.topic.4"/></a></li>
                                <li><a href="/global/topics/youth-employment/lang--${pageLocale.currentLocale.language}/index.htm"><fmt:message key="footer.topic.5"/></a></li>
                            </ul>
                        </li>
                        <li><a href="/global/publications/lang--${pageLocale.currentLocale.language}/index.htm"><fmt:message key="footer.publication.0"/></a>
                            <ul>
                                <li><a href="/global/publications/books/lang--${pageLocale.currentLocale.language}/index.htm"><fmt:message key="footer.publication.1"/></a></li>
                                <li><a href="/global/publications/working-papers/lang--${pageLocale.currentLocale.language}/index.htm"><fmt:message key="footer.publication.2"/></a></li>
                                <li><a href="/global/publications/magazines-and-journals/lang--${pageLocale.currentLocale.language}/index.htm"><fmt:message key="footer.publication.3"/></a></li>
                                <li><a href="/global/publications/meeting-reports/lang--${pageLocale.currentLocale.language}/index.htm"><fmt:message key="footer.publication.4"/></a></li>
                                <li><a href="/global/publications/ilo-bookstore/lang--${pageLocale.currentLocale.language}/index.htm"><fmt:message key="footer.publication.5"/></a></li>
                            </ul>
                        </li>
                    </ul>
                    <ul class="large-4-col">
                        <li><fmt:message key="footer.language.0"/>
                            <ul>
                                <li><a href="/beirut">اللغة العربية</a></li>
                                <li><a href="/beijing">中文</a></li>
                                <li><a href="/berlin">Deutsch</a></li>
                                <li><a href="/rome">Italiano</a></li>
                                <li><a href="/tokyo">日本語</a></li>
                                <li><a href="/brussels/lang--nl/index.htm">Nederlands</a></li>
                                <li><a href="/lisbon">Portugues</a></li>
                                <li><a href="/moscow">Русcкий</a></li>
                                <li><a href="/ankara">Türkçe </a></li>
                            </ul>
                        </li>
                    </ul>
                </nav>

            </div>
        </div>

    </footer>
</div>
<a href="#banner" class="back-to-top"><fmt:message key="footer.backtothetop"/></a>



<script src="<c:url value="/assets/js/plugins-ck.js"/>"></script>
<script src="<c:url value="/assets/js/fancybox/jquery.fancybox.js"/>"></script>
<script src="<c:url value="/assets/js/fancybox/helpers/jquery.fancybox-thumbs.js"/>"></script>
<script src="<c:url value="/assets/js/jMonthCalendar.js"/>"></script>
<script src="<c:url value="/assets/js/jquery.easydropdown.min.js"/>"></script>
<script src="<c:url value="/assets/js/tinynav.min.js"/>"></script>

<script src="assets/js/onload-ck.js"></script>



<!--[if lt IE 9]>
<script src="<c:url value="/assets/js/selectivizr-min.js"/>"></script>
<![endif]-->
