﻿<%@ page language="java" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" session="false" %>
<%@ include file="/WEB-INF/jsp/inc/include.jsp" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<c:url value="/assets/images/templates/favicons/msapplication-tileimage.png"/>">


    <title><fmt:message key="search.title" /></title>


    <link rel="stylesheet" media="all" href="<c:url value="/assets/css/screen.css"/>"/>
    <link rel="stylesheet" media="all" href="<c:url value="/css/search.css"/>"/>
    <link rel="stylesheet" media="all" href="<c:url value="/css/jquery.autocomplete.css"/>"/>
    <link rel="stylesheet" href="<c:url value="/assets/js/fancybox/jquery.fancybox.css"/>" type="text/css" media="screen" />

    <link rel="apple-touch-icon-precomposed" href="<c:url value="/assets/images/templates/favicons/apple-touch-icon-precomposed.png"/>">
    <link rel="icon" href="<c:url value="/assets/images/templates/favicons/favicon.png"/>">
    <link rel="shortcut icon" href="<c:url value="/assets/images/templates/favicons/favicon.ico"/>">


    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script src="<c:url value="/js/jquery.elemsort.js"/>"></script>
    <script src="<c:url value="/js/jquery.autocomplete.js"/>"></script>
    <script>
        if (/^(iamake.no-ip.org|odyssey.ventek.local)/.test(document.location.host))
            WebFontConfig = { fontdeck: { id: '45689' } };
        else
            WebFontConfig = { fontdeck: { id: '41991' } };

        (function() {
            var wf = document.createElement('script');
            wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
                    '://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
            wf.type = 'text/javascript';
            wf.async = 'true';
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(wf, s);
        })();
    </script>
    <script>
        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-4747079-1']);
        _gaq.push(['_trackPageview']);

        (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true; ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0];	s.parentNode.insertBefore(ga, s);
        })();
    </script>


    <!--[if IE 8]><link rel="stylesheet" media="screen" href="<c:url value="/assets/css/ie8.css"/>"><![endif]-->
    <!--[if lt IE 9]><script src="<c:url value="/assets/js/super-shiv.js"/>"></script><![endif]-->
</head>

<body class="lang-${pageLocale.currentLocale.language}">
<c:import url="inc/header.jsp"/>

<main class="container" role="main">
    <div class="row row-search-control">
        <div class="large-3-col hide-medium hide-small"></div>
        <div class="large-9-col medium-12-col small-12-col">
            <form method="get" action="<c:url value="/search.do" />">
                <h2><fmt:message key="search.label.searchbar"/></h2>

                <c:if test="${!fn:contains('ar,dv,fa,ha,he,iw,ji,ps,ur,yi', pageLocale.currentLocale.language)}">
                    <input type="hidden" name="searchLanguage" value="${pageLocale.currentLocale.language}"/>
                </c:if>

                <div class="wrapper-sort">
                    <button class="sort">
                        <span class="glyphicon glyphicon-sort-by-attributes"></span>
                        <span class="label" style="text-transform:capitalize">&darr;&nbsp;<fmt:message key="search.label.sortby.${httpRequest.sortby}"/></span>
                    </button>
                    <ul>
                        <li>
                            <c:url value="${httpRequest.searchURI}" var="url">
                                <c:param name="searchWhat" value="${httpRequest.searchWhat}"/>
                                <c:param name="navigators" value="${httpRequest.navigators}"/>
                                <c:param name="sortby" value="default"/>
                                <c:param name="lastDay" value="${httpRequest.lastDay}"/>
                                <c:param name="collection" value="${httpRequest.collection}"/>
                                <c:param name="offset" value="0"/>
                            </c:url>
                            <a href="${url}"><fmt:message key="search.label.sortby"/>&nbsp;<fmt:message key="search.label.sortby.default"/></a>
                        </li>
                        <li>
                            <c:url value="${httpRequest.searchURI}" var="url">
                                <c:param name="searchWhat" value="${httpRequest.searchWhat}"/>
                                <c:param name="navigators" value="${httpRequest.navigators}"/>
                                <c:param name="sortby" value="docdatetime"/>
                                <c:param name="lastDay" value="${httpRequest.lastDay}"/>
                                <c:param name="collection" value="${httpRequest.collection}"/>
                                <c:param name="offset" value="0"/>
                            </c:url>
                            <a href="${url}"><fmt:message key="search.label.sortby"/>&nbsp;<fmt:message key="search.label.sortby.docdatetime"/></a>
                        </li>
                    </ul>
                </div>
                <script>
                    (function($) {
                        var $dropdown = $('.wrapper-sort ul');

                        $(document)
                                .on('mouseenter', '.wrapper-sort', function() {
                                    $dropdown.show();
                                })
                                .on('mouseleave', '.wrapper-sort', function() {
                                    $dropdown.hide();
                                })
                        ;
                    })(jQuery);
                </script>
                <!-- TODO: ILO CR1 -->
                <div class="wrapper-submit">
                    <button type="submit" class="submit"><span class="label"><fmt:message key="search.label.search"/></span></button>
                </div>
                <div class="wrapper-query">
                    <input type="text" class="query" name="searchWhat" value="<c:out value="${httpRequest.searchWhat}"/>"/>
                </div>
            </form>
        </div>
    </div>

    <div class="row">
        <div class="large-3-col medium-12-col small-12-col">
            <c:if test="${fn:length(results.navigation) > 0}">
                <nav role="navigation" class="sub-nav sub-nav-search">
                    <h4>
                        <a href="#refine-toggle"><i class="icon-angle-up"></i><i class="icon-angle-down" style="display:none;"></i></a>
                        <fmt:message key="search.refinesearch-by"/>
                    </h4>

                    <c:forEach items="${results.navigation}" var="navigationEntry">
                        <c:set var="navigation" value="${navigationEntry.value}"/>
                        <div class="nav-refine nav-refine-${navigation.name}">
                            <h5><fmt:message key="${navigation.name}"/></h5>
                            <ul>
                                <c:forEach items="${navigation.modifiers}" var="modifier">
                                    <c:url value="${httpRequest.searchURI}" var="url">
                                        <c:param name="searchWhat" value="${httpRequest.searchWhat}"/>
                                        <c:param name="navigators" value="${modifier.refinestr}"/>
                                        <c:param name="sortby" value="${httpRequest.sortby}"/>
                                        <c:param name="lastDay" value="${httpRequest.lastDay}"/>
                                        <c:param name="collection" value="${httpRequest.collection}"/>
                                        <c:param name="offset" value="0"/>
                                    </c:url>
                                    <li><a href="${url}"><spring:message code="translate.${modifier.modifier.navName}.${modifier.name}" text="${modifier.name}"/> (${modifier.count})</a></li>
                                </c:forEach>
                                <c:if test="${fn:length(navigation.modifiers) > 5}">
                                    <li class="more">
                                        <a href="#">
                                            <fmt:message key="search.navigator.more" />
                                            <span> <fmt:message key="${navigation.name}_s"/></span>
                                        </a>
                                    </li>
                                    <li class="more" style="display:none">
                                        <a href="#">
                                            <fmt:message key="search.navigator.less" />
                                            <span> <fmt:message key="${navigation.name}"/></span>
                                        </a>
                                    </li>
                                </c:if>
                            </ul>
                        </div>
                    </c:forEach>
                </nav>
            </c:if>
        </div>

        <div class="large-9-col medium-12-col small-12-col">
            <div class="search-summary">
                <div class="subscribe hide-medium hide-small">
                    <c:url var="url" value="${httpRequest.searchURI}">
                        <c:param name="action">rss</c:param>
                        <c:param name="searchWhat" value="${httpRequest.searchWhat}"/>
                        <c:param name="navigators" value="${httpRequest.navigators}"/>
                        <c:param name="sortby" value="${httpRequest.sortby}"/>
                        <c:param name="lastDay" value="${httpRequest.lastDay}"/>
                        <c:param name="collection" value="${httpRequest.collection}"/>
                        <c:param name="locale" value="${pageLocale.currentLocale}"/>
                    </c:url>
                    <a href="${url}">
                        <fmt:message key="search.label.rss" />
                        <img src="<c:url value="/assets/images/templates/icon-rss.png"/>">
                    </a>
                </div>

                <c:choose>
                    <c:when test="${fn:length(results.docs) > 0}">
                        <fmt:formatNumber pattern="#,### " value="${results.pagination.itemTotal}"/>
                        <fmt:message key="search.label.results-found-for"/>
                        <span style="font-weight:bold"><c:out value=" ${httpRequest.searchWhat} "/></span>
                        <c:if test="${(fn:length(results.breadcrumb) > 0) || (httpRequest.lastDay gt 0)}">
                            <fmt:message key="search.label.match-criteria" />:
                        </c:if>
                    </c:when>
                    <c:when test="${results.pagination.itemTotal > 0}">
                        <fmt:message key="search.label.results.outofbound" />
                    </c:when>
                    <c:when test="${results.pagination.itemTotal == null}">
                        <fmt:message key="search.label.please-enter-a-search-term" />
                    </c:when>
                    <c:otherwise>
                        <fmt:message key="search.label.results.notfound" />
                        <ul class="bullet">
                            <li><fmt:message key="search.label.results.notfound.1" /></li>
                            <li><fmt:message key="search.label.results.notfound.2" /></li>
                        </ul>
                    </c:otherwise>
                </c:choose>

                <c:if test="${(fn:length(results.breadcrumb) > 0) || (httpRequest.lastDay gt 0)}">
                    <ul class="filtered">
                        <c:forEach var="breadcrumb" items="${results.breadcrumb}">
                            <c:url value="${httpRequest.searchURI}" var="url">
                                <c:param name="searchWhat" value="${httpRequest.searchWhat}"/>
                                <c:param name="navigators" value="${breadcrumb.refinestr}"/>
                                <c:param name="sortby" value="${httpRequest.sortby}"/>
                                <c:param name="lastDay" value="${httpRequest.lastDay}"/>
                                <c:param name="collection" value="${httpRequest.collection}"/>
                                <c:param name="offset" value="0"/>
                            </c:url>
                            <li>
                                <a href="${url}" title="<fmt:message key="search.label.remove-this.keyword"/>">
                                    <spring:message code="translate.${breadcrumb.modifier.navName}.${breadcrumb.name}" text="${breadcrumb.name}"/>
                                </a>
                            </li>
                        </c:forEach>
                    </ul>
                    <div>
                        (<fmt:message key="search.label.click-to-remove-criteria" />)
                    </div>
                </c:if>
            </div>

            <c:choose>
                <c:when test="${fn:length(results.didyoumean) > 0}">
                    <c:set var="_didyoumean" value="${fn:replace(results.didyoumean, '\"', '')}"/>
                    <c:set var="didyoumean" value="${results.didyoumean}"/>
                    <c:if test="${httpRequest.searchWhat != _didyoumean}">
                        <div class="search-didyoumean">
                            <c:url value="${httpRequest.searchURI}" var="url">
                                <c:param name="searchWhat" value="${results.didyoumean}"/>
                                <c:param name="navigators" value="${httpRequest.navigators}"/>
                                <c:param name="sortby" value="${httpRequest.sortby}"/>
                                <c:param name="lastDay" value="${httpRequest.lastDay}"/>
                                <c:param name="collection" value="${httpRequest.collection}"/>
                                <c:param name="offset" value="0"/>
                            </c:url>
                            <fmt:message key="search.label.didyoumean" />
                            <a href="${url}" style="font-style: italic;"> ${results.didyoumean}</a> ?
                        </div>
                    </c:if>
                </c:when>
                <c:when test="${results.phraseDocCount != null}">
                    <div class="search-didyoumean">
                        <c:url value="${httpRequest.searchURI}" var="url">
                            <c:param name="searchWhat" value="\"${httpRequest.searchWhat}\""/>
                            <c:param name="navigators" value="${httpRequest.navigators}"/>
                            <c:param name="sortby" value="${httpRequest.sortby}"/>
                            <c:param name="lastDay" value="${httpRequest.lastDay}"/>
                            <c:param name="collection" value="${httpRequest.collection}"/>
                            <c:param name="offset" value="0"/>
                        </c:url>
                        <fmt:message key="search.label.try-searching.1" />
                        &quot;<a href="${url}" style="font-style: italic;">${httpRequest.searchWhat}</a>&quot;
                        <fmt:message key="search.label.try-searching.2" />
                        <span> ${results.phraseDocCount} </span>
                        <fmt:message key="search.label.try-searching.3" />
                    </div>
                </c:when>
            </c:choose>

            <c:if test="${fn:length(results.bb_docs) > 0}">
                <div class="search-bestbet">
                    <div class="title"><fmt:message key="search.label.editor-choice"/></div>
                    <ol>
                        <c:forEach var="doc" items="${results.bb_docs}">
                            <c:choose>
                                <c:when test="${doc.collection == 'ILOLaborDoc'}">
                                    <c:url var="docurl" value="/marcdetails.do" >
                                        <c:param name="marcid" value="${doc.url}"/>
                                    </c:url>
                                    <li><a class="large" href="${docurl}">${doc.title}</a></li>
                                </c:when>
                                <c:otherwise>
                                    <li><a class="large" href="<c:url value="${doc.url}"/>">${doc.title}</a></li>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </ol>
                </div>
            </c:if>

            <div class="search-result">
                <c:forEach var="doc" items="${results.docs}">
                    <c:choose>
                        <c:when test="${doc.collection == 'ILOLaborDoc'}">
                            <div class="results ${doc.language != null && fn:length(doc.language) > 0 && fn:contains('ar,dv,fa,ha,he,iw,ji,ps,ur,yi', doc.language) ? 'rtl' : 'ltr'}" dir="${doc.language != null && fn:length(doc.language) > 0 && fn:contains('ar,dv,fa,ha,he,iw,ji,ps,ur,yi', doc.language) ? 'rtl' : 'ltr'}">
                                <div class="text">
                                    <c:url var="docurl" value="/marcdetails.do" >
                                        <c:param name="marcid" value="${doc.url}"/>
                                    </c:url>
                                    <h2>
                                        <a class="large" href="${docurl}">${doc.title}</a>
                                        <c:if test="${fn:length(doc.attachment) > 0}">
                                            <a class="" href="<c:url value="${doc.attachment}"/>">
                                                <img src="<c:url value="/images/ic-pdf.gif"/>" alt="<fmt:message key="search.label.attachment"/>" style="border: 0"/>
                                            </a>
                                        </c:if>
                                    </h2>
                                    <span class="teaser">
                                            <c:out value="${doc.body}" escapeXml="false" />
                                    </span>
                                    <span class="date">
                                        <c:if test="${fn:length(doc.publisher) > 0 || doc.pubyear != null}">
                                            <fmt:message key="search.label.published"/>: ${doc.publisher} ${doc.pubyear}
                                        </c:if>
                                    </span>
                                    <span class="category">
                                        <c:if test="${fn:length(doc['class']) > 0}">
                                            [${doc['class']}]
                                        </c:if>

                                        <c:if test="${fn:length(doc.marcurl) > 0}">
                                            [<a href="${doc.marcurl}">
                                            <c:choose>
                                                <c:when test="${fn:endsWith(fn:toLowerCase(doc.marcurl), 'pdf')}">
                                                    PDF
                                                </c:when>
                                                <c:when test="${fn:endsWith(fn:toLowerCase(doc.marcurl), 'htm') || fn:endsWith(fn:toLowerCase(doc.marcurl), 'html')}">
                                                    Web Page
                                                </c:when>
                                                <c:otherwise>
                                                    Document
                                                </c:otherwise>
                                            </c:choose>
                                            </a>]
                                        </c:if>
                                    </span>
                                    <span class="link"><a class="" target="_blank" href="${docurl}">MARC: <c:out value="${doc.url}"/></a></span>
                                </div>
                                <div class="clear-right"></div>
                            </div>
                        </c:when>
                        <c:otherwise>
                            <div class="doc ${doc.language != null && fn:length(doc.language) > 0 && fn:contains('ar,dv,fa,ha,he,iw,ji,ps,ur,yi', doc.language) ? 'rtl' : 'ltr'}" dir="${doc.language != null && fn:length(doc.language) > 0 && fn:contains('ar,dv,fa,ha,he,iw,ji,ps,ur,yi', doc.language) ? 'rtl' : 'ltr'}">
                                <c:if test="${fn:length(doc.thumbnail) > 0}">
                                    <div class="thumbnail">
                                        <img src="<c:url value="${doc.thumbnail}"/>"/>
                                    </div>
                                </c:if>

                                <h2>
                                    <a href="<c:url value="${doc.url}"/>">
                                        <c:out value="${doc.title}" escapeXml="false"/>
                                    </a>
                                </h2>
                                <div class="teaser">
                                    <c:out value="${doc.body}" escapeXml="false" />
                                </div>

                                <c:if test="${fn:length(doc.attachment) > 0}">
                                    <div class="attachment">
                                        <fmt:message key="search.label.download-the"/>
                                        <a href="<c:url value="${doc.attachment}"/>">
                                            [PDF <fmt:formatNumber type="number" pattern="#,###" value="${doc.size / 1024}"/> KB]
                                        </a>
                                    </div>
                                </c:if>

                                <div class="link">
                                    <span class="time"><fmt:formatDate value="${doc.docdatetime}" type="date" dateStyle="long"/></span>

                                    <c:if test="${fn:length(doc.type) > 0}">
                                        <span class="category"><fmt:message key="translate.ilotypenavigator.${doc.type}"/></span>
                                    </c:if>
                                </div>

                                <%--
                                <div class="link">
                                    <c:out value="${doc.url}"/>

                                    <c:if test="${fn:length(doc.type) > 0}">
                                        <span class="category">[${doc.type}]</span>
                                    </c:if>
                                </div>
                                --%>

                                <%-- This part is for search preview --%>
                                <%--
                                <c:if test="${fn:length(doc.viewsourceurl) > 0}">
                                    <%
                                        StringTokenizer tokenizer = new StringTokenizer(request.getParameter("searchWhat"));
                                        StringBuilder sb = new StringBuilder();
                                        while (tokenizer.hasMoreTokens()) {
                                            if (sb.length() > 0)
                                                sb.append(",");
                                            sb.append("content:");
                                            sb.append(URLEncoder.encode("\"" + tokenizer.nextToken().trim() + "\"", "UTF-8"));
                                        }
                                        String viewSourceUrlQuery = "AND(" + sb.toString() + ")";
                                    %>
                                    &nbsp;&nbsp;<span class="category" style="font-size:0.95em;"><a class="preview" href="<c:url value="${fn:replace(doc.viewsourceurl, 'odyssey.ventek.local', 'bangkok.ventek.ch') }"/>&qtf_teaser:query=<%= viewSourceUrlQuery %>">preview</a></span>
                                </c:if>
                                --%>
                            </div>
                        </c:otherwise>
                    </c:choose>

                    <%--
                    <div class="container-preview" style="position:fixed;top:0;right:0;background-color:#f7f7f7;">
                        <div style="width:400px;height:600px;margin:10px;">
                            <iframe style="background-color:#fff;width:600px;height:900px;-moz-transform: scale(0.6667, 0.6667);-webkit-transform: scale(0.6667, 0.6667);-o-transform: scale(0.6667, 0.6667);-ms-transform: scale(0.6667, 0.6667);transform: scale(0.6667, 0.6667);-moz-transform-origin: top left;-webkit-transform-origin: top left;-o-transform-origin: top left;-ms-transform-origin: top left;transform-origin: top left;" frameborder="0"></iframe>
                        </div>
                    </div>

                    <script>
                        (function($) {
                            $(document)
                                    .on('mouseover', 'a.preview', function(e) {
                                        var $currentTarget = $(e.currentTarget);
                                        var url = $currentTarget.attr('href');

                                        var $preview_iframe = $('.container-preview iframe');
                                        $preview_iframe.attr('src', url);
                                    });
                        })(jQuery);
                    </script>
                    --%>
                </c:forEach>
            </div>

            <c:if test="${fn:length(results.pagination.pages) > 0}">
                <div class="search-pagination">
                    <div class="page-option">
                        <c:if test="${results.pagination.prev != null}">
                            <c:url value="${httpRequest.searchURI}" var="url">
                                <c:param name="searchWhat" value="${httpRequest.searchWhat}"/>
                                <c:param name="navigators" value="${httpRequest.navigators}"/>
                                <c:param name="sortby" value="${httpRequest.sortby}"/>
                                <c:param name="lastDay" value="${httpRequest.lastDay}"/>
                                <c:param name="collection" value="${httpRequest.collection}"/>
                                <c:param name="offset" value="${results.pagination.prev.offset}"/>
                            </c:url>
                            <a href="${url}">&lt; <fmt:message key="search.label.prev"/></a>
                        </c:if>
                        <c:forEach items="${results.pagination.pages}" var="page" varStatus="status">
                            <c:choose>
                                <c:when test="${page.current}">
                                    <a href="#" class="selected">${page.page}</a>
                                </c:when>
                                <c:otherwise>
                                    <c:url value="${httpRequest.searchURI}" var="url">
                                        <c:param name="searchWhat" value="${httpRequest.searchWhat}"/>
                                        <c:param name="navigators" value="${httpRequest.navigators}"/>
                                        <c:param name="sortby" value="${httpRequest.sortby}"/>
                                        <c:param name="lastDay" value="${httpRequest.lastDay}"/>
                                        <c:param name="collection" value="${httpRequest.collection}"/>
                                        <c:param name="offset" value="${page.offset}"/>
                                    </c:url>
                                    <a href="${url}">${page.page}</a>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                        <c:if test="${results.pagination.next != null}">
                            <c:url value="${httpRequest.searchURI}" var="url">
                                <c:param name="searchWhat" value="${httpRequest.searchWhat}"/>
                                <c:param name="navigators" value="${httpRequest.navigators}"/>
                                <c:param name="sortby" value="${httpRequest.sortby}"/>
                                <c:param name="lastDay" value="${httpRequest.lastDay}"/>
                                <c:param name="collection" value="${httpRequest.collection}"/>
                                <c:param name="offset" value="${results.pagination.next.offset}"/>
                            </c:url>
                            <a href="${url}"><fmt:message key="search.label.next"/> &gt;</a>
                        </c:if>
                    </div>

                    <div class="page-status">
                        <fmt:message key="search.pagination.page"/>
                        <fmt:formatNumber pattern=" #,### " value="${results.pagination.current.page}"/>
                        <fmt:message key="search.pagination.of"/>
                        <fmt:formatNumber pattern=" #,###" value="${1 + results.pagination.itemTotal / 10 - (results.pagination.itemTotal / 10 % 1)}"/>
                    </div>
                </div>
            </c:if>
        </div>
    </div>
</main>

<c:import url="inc/footer.jsp"/>

<script>
    (function() {
        $('.nav-refine-search\\.navigator\\.date\\.title li').not('.more').sortElements(function(a, b){
            return - /(.*) \(\d+\)/.exec($(a).text().toLowerCase())[1].localeCompare(/(.*) \(\d+\)/.exec($(b).text().toLowerCase())[1]);
        });

        $('.nav-refine-search\\.navigator\\.theme\\.title li').not('.more').sortElements(function(a, b){
            return /(.*) \(\d+\)/.exec($(a).text().toLowerCase())[1].localeCompare(/(.*) \(\d+\)/.exec($(b).text().toLowerCase())[1]);
        });

        $('.nav-refine-search\\.navigator\\.language\\.title li').not('.more').sortElements(function(a, b){
            return /(.*) \(\d+\)/.exec($(a).text().toLowerCase())[1].localeCompare(/(.*) \(\d+\)/.exec($(b).text().toLowerCase())[1]);
        });

        $('.nav-refine').find('li:gt(4)').not('.more').hide();
        $('.nav-refine').find('.more').bind('click', function(e) {
            $( e.currentTarget ).parent().find('li:gt(4)').slideToggle();
            e.preventDefault();
            return false;
        });

        $('a[href^="#refine-toggle"]').on('click', function(e) {
            var $currentTarget = $(e.currentTarget);

            $currentTarget.find('i').toggle();
            $('.nav-refine').slideToggle();

            e.preventDefault();
            return false;
        });

        <%--
        // Uniformly randomly swap tag cloud
        // RANDOMIZE-IN-PLACE (Pg. 93 Thomas Cormen, Charles Leiserson, Ronale Rivest. Introduction to Algorithm, 2nd Edition.)
        var i, j;
        var len = $('#divTagCloud .tc').length;
        for ( i = 0; i < len; i++ ) {
            j = i + parseInt(Math.random() * (len - i));
            if ( i < j ) { // swap i, j
                $($('#divTagCloud .tc')[i]).insertBefore( $('#divTagCloud .tc')[j] );
                $($('#divTagCloud .tc')[j]).insertBefore( $('#divTagCloud .tc')[i] );
            }
        }
        --%>


        $('input.query').autocomplete(
                '<c:url value="/suggest.do"/>',
                {	matchContains: true,
                    max: 10,
                    minChars: 2,
                    cacheLength: 0,
                    scrollHeight: 200,
                    selectFirst: false,
                    extraParams: {
                        lang: '${pageLocale.currentLocale.language}'
                    }
                });
    })();
</script>
</body>
</html>