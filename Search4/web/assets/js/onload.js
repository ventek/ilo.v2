/* Flexslider
------------------------------------------------- */

if ($("#slider").length > 0) {
   
    $('#slider').flexslider({
        animation: "fade",
        controlNav: true,
        animationLoop: false,
        slideshow: false,
        start: function(slider){
            if ($(".thumb-carousel").length > 0) {
                var thumbs = $(".thumb-carousel ul li").on("click touchstart", function(e){
                    e.preventDefault();
                    var ind = thumbs.index(this);
                    slider.flexAnimate(ind);
                });
            }
        },
        after: function(slider){
            var slide = 3;
            var inView = slider.currentSlide + 1;
            var marginTop = 0;
            var thumbHeight = $(".thumb-carousel ul li").height();
            var thumbMarg = parseInt($(".thumb-carousel ul li").css("margin-bottom").replace("px",""));
            if(inView > 0)
                slide = Math.ceil(inView/3.0) * 3;
            else if( inView < 0)
                slide = Math.floor(inView/3.0) * 3;
            else
                slide = 3;
            slide = slide - 3;
            if(slide == 0)
               $(".thumb-carousel ul").css({"margin-top":marginTop});
            else
               // alert(thumbMarg);
               marginTop = (thumbHeight + thumbMarg) * slide;
               $(".thumb-carousel ul").css({"margin-top":"-" + marginTop + "px"});
            $(".thumb-carousel ul li").removeClass("active");
            $(".thumb-carousel ul li:nth-child(" + inView + ")").addClass("active");
        }
    });

    function posControls() {
        if( $(window).width() <= 767 ) {
            var pos_top = ($('#slider .slides > li.flex-active-slide > img').height() / 2) - 15; // next/prev pos top
            var c_pos_top = ($('#slider .slides > li.flex-active-slide > img').height() / 2) - ($('#slider .thumb-carousel').height() / 2); // thumbs carousel pos top
            $('#slider .thumb-carousel').css({"top":c_pos_top + "px"});
        } else {    
            var pos_top = ($('#slider .slides > li.flex-active-slide > img').height() / 2); // next/prev pos top
            $('#slider .thumb-carousel').attr("style","");
        }
        $('#slider .flex-next, #slider .flex-prev').css({"margin-top":"0","top":pos_top + "px"});
    }
    $(window).resize(function() { posControls(); });
    $(window).load(function() { posControls(); });

}

if ($(".slider-quote").length > 0) {
    $('.slider-quote').flexslider({
        animation: "fade",
        controlNav: true,
        animationLoop: true,
        slideshow: false
    });
}

if ($("#slider-large").length > 0) {
   
    $('#slider-large').flexslider({
        animation: "fade",
        controlNav: true,
        animationLoop: false,
        slideshow: false,
        start: function(slider){
            if ($(".thumb-text-carousel").length > 0) {
                var thumbs = $(".thumb-text-carousel ul li").on("click touchstart", function(e){
                    e.preventDefault();
                    var ind = thumbs.index(this);
                    slider.flexAnimate(ind);
                });
            }
        },
        after: function(slider){
            if($(".thumb-text-carousel").length > 0) {
                var slide = 4;
                var inView = slider.currentSlide + 1;
                var marginTop = 0;
                var thumbHeight = $(".thumb-text-carousel ul li").height();
                var thumbMarg = parseInt($(".thumb-text-carousel ul li").css("margin-bottom").replace("px",""));
                if(inView > 0)
                    slide = Math.ceil(inView/4.0) * 4;
                else if( inView < 0)
                    slide = Math.floor(inView/4.0) * 4;
                else
                    slide = 4;
                slide = slide - 4;
                if( $(window).width() > 767 ) {
                    if(slide == 0)
                        $(".thumb-text-carousel ul").css({"margin-top":marginTop});
                    else
                        marginTop = thumbHeight * slide;
                        $(".thumb-text-carousel ul").css({"margin-top":"-" + marginTop + "px"});
                        $(".thumb-text-carousel ul li").removeClass("active");
                        $(".thumb-text-carousel ul li:nth-child(" + inView + ")").addClass("active");
                } else {
                    // move horizontally instead !!
                }
            }
        }
    });

    function sizeCarousel() {
        $(".thumb-text-carousel").height( $('#slider-large .slides > li.flex-active-slide > img').height() );
        $(".thumb-text-carousel li").each(function(){
            var that = $(this);
            $(this).find("p").height($(that).find("img").height()-10);
        });
        var pos_top = ($('#slider-large .slides > li.flex-active-slide > img').height() / 2) - 15;
        $('#slider-large .flex-next, #slider-large .flex-prev').css({"margin-top":"0","top":pos_top + "px"});
    }
    $(window).resize(function() { sizeCarousel(); });
    $(window).load(function() { sizeCarousel(); });

}

/* Tiny Nav
------------------------------------------------- */

$(".sub-nav ol").tinyNav({
  header: 'In this section...', // String: Specify text for "header" and show header instead of the active item
});


/* Links
------------------------------------------------- */

// add link arrows
$("#main-content a").not(".item-image, .addthis_btn, .carousel-control, .more-tools, .fancybox-thumb").each(function(){
    if($("body").attr("dir") == "rtl") {
        $(this).append('&nbsp;<span class="linked icon-angle-left"></span>');
    } else {
	   $(this).append('&nbsp;<span class="linked icon-angle-right"></span>');
    }
});

// sub nav dropdowns
$(".sub-nav .dropdown").not(".open").find("ol").hide().end().append("<a href='#' class='expand'>[+]</a>");
$(".sub-nav .dropdown.open").append("<a href='#' class='expand'>[-]</a>");

$(".expand").on("click",function(e){
	e.preventDefault();
	var that = $(this);
	$(this).parent().find("> ol").slideToggle("slow", function() {
    	if($(that).html() == "[+]") {
    		$(that).html("[-]");
            $(that).parent().find(".dropdown-menu").addClass("open");
    	} else {
    		$(that).html("[+]");
            $(that).parent().find(".dropdown-menu").removeClass("open");
    	}
    });
});


/* Page Tools
------------------------------------------------- */

// init addthis asynchronously
if ( $(".page-share").length > 0 ) {
	$(window).load(function() {
	    addthis.init();
	});
}

// position sticky
if ( $(".sticky").length > 0 ) {
    if( $(".post-footer").length > 0 ) {
	   $("article.main-article .post-footer").after("<div class='stickies'></div>");
    } else {
        $("article.main-article header").after("<div class='stickies'></div>");
    }
	function posSticky() {
		if( $(window).width() > 979 ) {
			$(".stickies").hide();
			$("aside.sidebar .sticky").show();
		} else {
			if ( $(".stickies").html() == "" ) {
				$("aside.sidebar .sticky").each(function(){
					$(".stickies").append( $(this).clone() );
				});
				addthis.init();
			}
			$(".stickies").show();
			$("aside.sidebar .sticky").hide();
		}
	}
	posSticky();
	$(window).resize(function() { posSticky(); });
}

// font size switcher
if ( $(".page-tools").length > 0 ) {
    $(".small-text").livequery("click", function(e){
        e.preventDefault();
        $("html").removeClass("font-medium").removeClass("font-large").addClass("font-small");
    });
    $(".medium-text").livequery("click", function(e){
        e.preventDefault();
        $("html").removeClass("font-small").removeClass("font-large").addClass("font-medium");
    });
    $(".large-text").livequery("click", function(e){
        e.preventDefault();
        $("html").removeClass("font-medium").removeClass("font-small").addClass("font-large");
    });
}

//Tool Toggle
if ( $(".page-tools").length > 0 ) {
    $( ".mobile-expand a" ).livequery("click", function(e) {
      e.preventDefault();
      $( ".tools-collapse" ).slideToggle( "10" , function(e) {
        if($(this).prev(".mobile-expand").find("span").html() == "[+]") {
            $(this).prev(".mobile-expand").find("span").html("[-]");
        } else {
            $(this).prev(".mobile-expand").find("span").html("[+]");
        }
      });
    });
}


/* White social icons
------------------------------------------------- */

if ($("body > div.artworks").length > 0) {
    $(".social-pages").addClass("bg-white");
}


/* Mega Dropdown
------------------------------------------------- */

var touch = ("ontouchstart" in window) || window.DocumentTouch && document instanceof DocumentTouch;

if(touch) {

    $(".navbar-nav .dropdown").on("click touchend", function(e){
        $(this).find(".mega-dropdown").slideDown("fast");
        e.stopPropagation();
    });

    $(document).on('click touchstart', function(e) {
        $('#banner .mega-dropdown').slideUp('fast');
    });

} else {

    $(".navbar-nav .dropdown").hoverIntent(function(){
    	$(this).find(".mega-dropdown").slideToggle("fast");
    });

}


/* Mobile Menu
------------------------------------------------- */

var jPM = $.jPanelMenu({
    menu: '#menu',
    trigger: '.navbar-toggle',
    keyboardShortcuts: false,
    afterOn: function() {
        $("#jPanelMenu-menu li:first-child").before( $(".social-pages ul").html() );
    	$("#jPanelMenu-menu li:last-child").before( $("#secondary-banner .navbar-nav").html() );
    	$("#jPanelMenu-menu .mega-dropdown").hide();
    	$("#jPanelMenu-menu .dropdown").on("click", function(e){
    		e.preventDefault();
    		$(this).find(".mega-dropdown").slideToggle("fast");
    	});
    }
});

jPM.on();


/* Deal with align right links in feature rich
------------------------------------------------- */

$(".feature-multiple .item-list > li > div[style='text-align: right']").parent("li").addClass("view-more-li");
$(".feature-multiple .item-list > li > div[style='text-align: right;']").parent("li").addClass("view-more-li"); // IE


/* Responsive videos
------------------------------------------------- */

$(".furniture").fitVids();


/* Affix social
------------------------------------------------- */

$(window).scroll(function() {
    var offset = $(window).scrollTop();
    var init_affix = $("#banner").height();
    var social = $(".social-pages"); 
    if(offset > init_affix) {
        $(social).addClass("affix");
    } else {
        $(social).removeClass("affix");
    }
});


/* Fancy Box
------------------------------------------------- */

if ($(".slideshow").length > 0) {

    $(".fancybox-thumb").fancybox({
        helpers : {
            title   : {
                type: 'outside'
            },
            thumbs  : {
                width   : 50,
                height  : 50
            }
        }
    });

}


/* Custom select
------------------------------------------------- */

$(function(){
    var $selects = $('.custom-select,select.#tinynav1');
    $selects.easyDropDown({
        cutOff: 10,
        wrapperClass: 'custom-select'
    });
});


/* Move homepage nav
------------------------------------------------- */

if ($(".homepage").length > 0 && $(window).width() >= 768 && $(window).width() <= 979) {
    $(".homepage > .container > .row > .large-3-col:first-child").prepend( $(".masthead .hide-medium").html() );
    $(".masthead .hide-medium").html("")
}


/* Calendar
------------------------------------------------- */

var options = {
    height: 160,
    width: 210,
    firstDayOfWeek: 1,
    navHeight: 15,
    labelHeight: 15,
    navLinks: {
    enableToday: false,
    enableNextYear: false,
    enablePrevYear: false,
    p:'<',
    n:'>',
    t:'Today',
    showMore: 'Show More'
    },
    onMonthChanging: function(dateIn) {
        return true;
    },
    onDayCellClick: function(date) { 
        alert(date.toLocaleDateString());
        return true; 
    }
}
locale: {
    days: ["sun", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"]
};

var events = [ ];
var newoptions = { };
var newevents = [ ];

$(window).load(function() {

    $.jMonthCalendar.Initialize(options, events);

});


/* Filter collapse
------------------------------------------------- */

if ($(".filter").length > 0) {
    $( ".filter-collapse" ).hide().before("<a href='#' class='filter-expand'><span>[+]</span></a>");
    $( ".filter-expand" ).livequery(function(){
        $(this).on("click", function(e) {
            e.preventDefault();
            var that = $(this);
            $(that).next( ".filter-collapse" ).slideToggle( "10" , function(e) {
                if($(this).prev(".filter-expand").find("span").html() == "[+]") {
                    $(this).prev(".filter-expand").find("span").html("[-]");
                } else {
                    $(this).prev(".filter-expand").find("span").html("[+]");
                }
            });
        });
    });
}
